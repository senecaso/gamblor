/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.lotto;

import com.google.common.base.Charsets;
import junit.framework.Assert;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPKeyRingGenerator;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Collections;
import java.util.Iterator;

public class TestGPGUtils {
	private static final char[] PASSWD = new char[] {'a'};
	private static final SecureRandom RAND = new SecureRandom();

	private File pubCollFile;
	private File secCollFile;
	private PGPPublicKeyRingCollection pubColl;
	private PGPSecretKeyRingCollection secColl;

	static {
		Security.addProvider(new BouncyCastleProvider());
	}

	@Before
	public void before() throws Exception {
		pubCollFile = File.createTempFile("pubCollFile", ".bin");
		secCollFile = File.createTempFile("secCollFile", ".bin");

		PGPKeyRingGenerator gen = GPGUtils.generateKeyRingGenerator("id", PASSWD, RAND);

		pubColl = new PGPPublicKeyRingCollection(Collections.singletonList(gen.generatePublicKeyRing()));
		secColl = new PGPSecretKeyRingCollection(Collections.singletonList(gen.generateSecretKeyRing()));
	}

	@After
	public void after() throws Exception {
		pubCollFile.delete();
		secCollFile.delete();
	}

	private PGPSecretKey getSecretKey() {
		Iterator<PGPSecretKeyRing> itr = secColl.getKeyRings();
		while (itr.hasNext()) {
			PGPSecretKeyRing ring = itr.next();
			return ring.getSecretKey();
		}

		return null;
	}

	private PGPPublicKey getPublicKey() {
		Iterator<PGPPublicKeyRing> itr = pubColl.getKeyRings();
		while (itr.hasNext()) {
			PGPPublicKeyRing ring = itr.next();
			return ring.getPublicKey();
		}

		return null;
	}

	@Test
	public void testSign() throws Exception {
		byte[] message = "hello world".getBytes(Charsets.UTF_8);
		byte[] signature = GPGUtils.signMessage(message, getSecretKey(), PASSWD, "SHA256", RAND);

		Assert.assertTrue(GPGUtils.verifyMessage(message, signature, pubColl));
	}
}
