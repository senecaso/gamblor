/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.lotto;

import com.google.common.base.Charsets;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Verifier {
	public static final String USAGE = Verifier.class.getSimpleName() + " <public file> <private file> <GPG public key ring collection file>";

	public static void main(String[] args) throws Exception {
		if (args.length != 3) {
			System.err.println(USAGE);
			System.exit(1);
		}

		File publicFile = new File(args[0]);
		File privateFile = new File(args[1]);

		JsonParser parser = new JsonParser();

		JsonObject publicJson = (JsonObject) parser.parse(new FileReader(publicFile));
		JsonObject privateJson = (JsonObject) parser.parse(new FileReader(privateFile));

		String drawDate = publicJson.get("drawDate").getAsString();

		List<Integer> numbers = new ArrayList<Integer>();
		for (JsonElement el : privateJson.get("numbers").getAsJsonArray()) {
			int n = el.getAsInt();
			if (n < 1 || n > 27) {
				System.err.println("winning numbers out of range [1-27]");
				System.err.println(USAGE);
				System.exit(1);
			}

			numbers.add(n);
		}

		if (numbers.size() != 3) {
			System.err.println("invalid number of winning numbers");
			System.err.println(USAGE);
			System.exit(1);
		}

		Collections.sort(numbers);

		byte[] secret = Base64.decodeBase64(privateJson.get("secret").getAsString());
		byte[] expectedSignature = publicJson.get("signature").getAsString().getBytes(Charsets.UTF_8);

		File gpgPublicKeyCollectionFile = new File(args[2]);
		PGPPublicKeyRingCollection pubColl = GPGUtils.loadPublicKeyRingCollection(gpgPublicKeyCollectionFile);

		try {
			MessageDigest sha256 = MessageDigest.getInstance("SHA-256");

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			baos.write(secret);
			baos.write(drawDate.getBytes(Charsets.UTF_8));
			for (Integer number : numbers) {
				baos.write((byte) number.intValue());
			}

			byte[] signatureData = baos.toByteArray();
			baos.close();

			for (int i = 0; i < Constants.HASH_ITERATIONS; i++) {
				signatureData = sha256.digest(signatureData);
			}

			if (!GPGUtils.verifyMessage(signatureData, expectedSignature, pubColl)) {
				System.out.println("VERIFICATION FAILED");
			} else {
				System.out.println("OK");
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.err.println(USAGE);
			System.exit(1);
		}
	}
}
