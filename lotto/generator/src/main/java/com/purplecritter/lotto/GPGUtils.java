/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.lotto;

import org.bouncycastle.bcpg.ArmoredInputStream;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.BCPGOutputStream;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.bcpg.sig.Features;
import org.bouncycastle.bcpg.sig.KeyFlags;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPKeyPair;
import org.bouncycastle.openpgp.PGPKeyRingGenerator;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureGenerator;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.bouncycastle.openpgp.PGPSignatureSubpacketGenerator;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.PBESecretKeyDecryptor;
import org.bouncycastle.openpgp.operator.PBESecretKeyEncryptor;
import org.bouncycastle.openpgp.operator.PGPContentSigner;
import org.bouncycastle.openpgp.operator.PGPContentSignerBuilder;
import org.bouncycastle.openpgp.operator.PGPDigestCalculator;
import org.bouncycastle.openpgp.operator.bc.BcPBESecretKeyDecryptorBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPBESecretKeyEncryptorBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPGPContentSignerBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPGPContentVerifierBuilderProvider;
import org.bouncycastle.openpgp.operator.bc.BcPGPDigestCalculatorProvider;
import org.bouncycastle.openpgp.operator.bc.BcPGPKeyPair;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.util.Date;
import java.util.Iterator;

public class GPGUtils {
	/**
	 * taken from http://bouncycastle-pgp-cookbook.blogspot.jp/2013/01/generating-rsa-keys.html
	 */
	public final static PGPKeyRingGenerator generateKeyRingGenerator(String id, char[] pass, SecureRandom rand) throws PGPException {
		// This object generates individual key-pairs.
		RSAKeyPairGenerator kpg = new RSAKeyPairGenerator();

		// Boilerplate RSA parameters, no need to change anything
		// except for the RSA key-size (2048). You can use whatever
		// key-size makes sense for you -- 4096, etc.
		kpg.init(new RSAKeyGenerationParameters(BigInteger.valueOf(0x10001), rand, 2048, 12));

		// First create the master (signing) key with the generator.
		PGPKeyPair rsakp_sign = new BcPGPKeyPair(PGPPublicKey.RSA_SIGN, kpg.generateKeyPair(), new Date());

		// Then an encryption subkey.
		PGPKeyPair rsakp_enc = new BcPGPKeyPair(PGPPublicKey.RSA_ENCRYPT, kpg.generateKeyPair(), new Date());

		// Add a self-signature on the id
		PGPSignatureSubpacketGenerator signhashgen = new PGPSignatureSubpacketGenerator();

		// Add signed metadata on the signature.
		// 1) Declare its purpose
		signhashgen.setKeyFlags(false, KeyFlags.SIGN_DATA | KeyFlags.CERTIFY_OTHER);

		// 2) Set preferences for secondary crypto algorithms to use
		//    when sending messages to this key.
		signhashgen.setPreferredSymmetricAlgorithms(false, new int[]{SymmetricKeyAlgorithmTags.AES_256, SymmetricKeyAlgorithmTags.AES_192, SymmetricKeyAlgorithmTags.AES_128});
		signhashgen.setPreferredHashAlgorithms(false, new int[]{HashAlgorithmTags.SHA256, HashAlgorithmTags.SHA1, HashAlgorithmTags.SHA384, HashAlgorithmTags.SHA512, HashAlgorithmTags.SHA224});

		// 3) Request senders add additional checksums to the
		//    message (useful when verifying unsigned messages.)
		signhashgen.setFeature(false, Features.FEATURE_MODIFICATION_DETECTION);

		// Create a signature on the encryption subkey.
		PGPSignatureSubpacketGenerator enchashgen = new PGPSignatureSubpacketGenerator();

		// Add metadata to declare its purpose
		enchashgen.setKeyFlags(false, KeyFlags.ENCRYPT_COMMS | KeyFlags.ENCRYPT_STORAGE);

		// Objects used to encrypt the secret key.
		PGPDigestCalculator sha1Calc = new BcPGPDigestCalculatorProvider().get(HashAlgorithmTags.SHA1);
		PGPDigestCalculator sha256Calc = new BcPGPDigestCalculatorProvider().get(HashAlgorithmTags.SHA256);

		// bcpg 1.48 exposes this API that includes s2kcount. Earlier
		// versions use a default of 0x60. 0xc0 is ~130K iterations
		PBESecretKeyEncryptor pske = (new BcPBESecretKeyEncryptorBuilder(PGPEncryptedData.AES_256, sha256Calc, 0xc0)).build(pass);

		// Finally, create the keyring itself. The constructor
		// takes parameters that allow it to generate the self
		// signature.
		PGPKeyRingGenerator keyRingGen =
				new PGPKeyRingGenerator
						(PGPSignature.POSITIVE_CERTIFICATION, rsakp_sign,
								id, sha1Calc, signhashgen.generate(), null,
								new BcPGPContentSignerBuilder
										(rsakp_sign.getPublicKey().getAlgorithm(),
												HashAlgorithmTags.SHA1),
								pske);

		// Add our encryption subkey, together with its signature.
		keyRingGen.addSubKey(rsakp_enc, enchashgen.generate(), null);

		return keyRingGen;
	}

	public static PGPSecretKeyRingCollection loadSecretKeyRingCollection(File file) throws IOException, PGPException {
		return new PGPSecretKeyRingCollection(new FileInputStream(file));
	}

	public static PGPPublicKeyRingCollection loadPublicKeyRingCollection(File file) throws IOException, PGPException {
		return new PGPPublicKeyRingCollection(new FileInputStream(file));
	}

	public static PGPSecretKey readSecretSigningKey(PGPSecretKeyRingCollection pgpSec, long signingKeyId) throws PGPException {
		PGPSecretKey key = pgpSec.getSecretKey(signingKeyId);
		if (key == null || !key.isSigningKey()) {
			throw new IllegalArgumentException("Can't find signing key in key ring.");
		}

		return key;
	}

	public static byte[] signMessage(byte[] message, PGPSecretKey pgpSecKey, char[] pass, String digestName, SecureRandom rand) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, PGPException, SignatureException {
		int digest;

		if (digestName.equals("SHA256")) {
			digest = PGPUtil.SHA256;
		} else if (digestName.equals("SHA384")) {
			digest = PGPUtil.SHA384;
		} else if (digestName.equals("SHA512")) {
			digest = PGPUtil.SHA512;
		} else if (digestName.equals("MD5")) {
			digest = PGPUtil.MD5;
		} else if (digestName.equals("RIPEMD160")) {
			digest = PGPUtil.RIPEMD160;
		} else {
			digest = PGPUtil.SHA1;
		}

		PGPPrivateKey pgpPrivKey = pgpSecKey.extractPrivateKey((new BcPBESecretKeyDecryptorBuilder(new BcPGPDigestCalculatorProvider())).build(pass));
		PGPSignatureGenerator sGen = new PGPSignatureGenerator((new BcPGPContentSignerBuilder(pgpSecKey.getPublicKey().getAlgorithm(), digest)).setSecureRandom(rand));
		PGPSignatureSubpacketGenerator spGen = new PGPSignatureSubpacketGenerator();

		sGen.initSign(PGPSignature.BINARY_DOCUMENT, pgpPrivKey);

		Iterator it = pgpSecKey.getPublicKey().getUserIDs();
		if (it.hasNext()) {
			spGen.setSignerUserID(false, (String) it.next());
			sGen.setHashedSubpackets(spGen.generate());
		}

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ArmoredOutputStream aOut = new ArmoredOutputStream(out);

		aOut.beginClearText(digest);

		sGen.update(message);

		aOut.endClearText();

		BCPGOutputStream bOut = new BCPGOutputStream(aOut);

		sGen.generate().encode(bOut);

		aOut.close();

		return out.toByteArray();
	}

	public static boolean verifyMessage(byte[] message, byte[] signature, PGPPublicKeyRingCollection pgpRings) throws IOException, PGPException, NoSuchProviderException, SignatureException {
		ArmoredInputStream aIn = new ArmoredInputStream(new ByteArrayInputStream(signature), true);

		// consume all of the header and cleartext bits
		while (aIn.read() >= 0 && aIn.isClearText()) {}

		// extract the signature
		PGPObjectFactory pgpFact = new PGPObjectFactory(aIn);
		PGPSignatureList p3 = (PGPSignatureList) pgpFact.nextObject();
		PGPSignature sig = p3.get(0);

		sig.init(new BcPGPContentVerifierBuilderProvider(), pgpRings.getPublicKey(sig.getKeyID()));
		sig.update(message);

		return sig.verify();
	}
}
