/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.lotto;

import com.google.common.base.Charsets;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.openpgp.PGPKeyRingGenerator;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRing;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

public class OfflineGenerator {
	private static final String USAGE = OfflineGenerator.class.getSimpleName() + " <firstDrawDate> <daysBetweenDraws> <num tickets> <public output file> <private output file> <GPG public key collection file> <GPG secret key collection file>";
	private static final DateFormat DF = new SimpleDateFormat("yyyy-MM-dd");

	private static final int SECRET_SIZE = 16;

	private static Set<Integer> pickNumbers(SecureRandom rand) {
		Set<Integer> numbers = new HashSet<Integer>();
		while (numbers.size() < 3) {
			numbers.add(rand.nextInt(27) + 1);
		}

		return numbers;
	}

	public static void main(String[] args) {
		SecureRandom rand = new SecureRandom();

		if (args.length != 7) {
			System.err.println(USAGE);
			System.exit(1);
		}

		Date firstDrawDate = null;
		int daysBetweenDraws = 0;
		int numTickets = 0;
		File publicFile = null;
		File privateFile = null;
		File gpgPublicKeyCollectionFile = null;
		File gpgSecretKeyCollectionFile = null;
		PGPPublicKeyRingCollection pubColl = null;
		PGPSecretKeyRingCollection secColl = null;
		char[] passwd = null;
		try {
			firstDrawDate = DF.parse(args[0]);
			daysBetweenDraws = Integer.parseInt(args[1]);
			numTickets = Integer.parseInt(args[2]);
			publicFile = new File(args[3]);
			privateFile = new File(args[4]);
			gpgPublicKeyCollectionFile = new File(args[5]);
			gpgSecretKeyCollectionFile = new File(args[6]);

			if (firstDrawDate.compareTo(new Date()) <= 0) {
				System.err.println("first draw date must be sometime in the future");
				System.err.println(USAGE);
				System.exit(1);
			}

			if (daysBetweenDraws <= 0) {
				System.err.println("there must be at least 1 day between each draw");
				System.err.println(USAGE);
				System.exit(1);
			}

			if (numTickets <= 0) {
				System.err.println("at least 1 ticket must be generated");
				System.err.println(USAGE);
				System.exit(1);
			}

			if (publicFile.exists()) {
				System.err.println("public output file already exists");
				System.err.println(USAGE);
				System.exit(1);
			}

			if (privateFile.exists()) {
				System.err.println("private output file already exists");
				System.err.println(USAGE);
				System.exit(1);
			}

			if (gpgPublicKeyCollectionFile.exists() && gpgSecretKeyCollectionFile.exists() ||
					!gpgPublicKeyCollectionFile.exists() && !gpgSecretKeyCollectionFile.exists()) {
				if (System.console() != null) {
					passwd = System.console().readPassword("Enter keystore password: ", new Object[0]);
				} else {
					System.out.println("CAUTION: No console detected.  Password will be displayed while entering");
					System.out.print("Enter keystore password: ");
					BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					String passwdStr = br.readLine();
					passwd = passwdStr.toCharArray();
					br.close();
				}

				if (gpgPublicKeyCollectionFile.exists() && gpgSecretKeyCollectionFile.exists()) {
					pubColl = GPGUtils.loadPublicKeyRingCollection(gpgPublicKeyCollectionFile);
					secColl = GPGUtils.loadSecretKeyRingCollection(gpgSecretKeyCollectionFile);
				} else {
					System.out.println("No GPG key collections found. Generating...");
					PGPKeyRingGenerator gen = GPGUtils.generateKeyRingGenerator("id", passwd, rand);
					pubColl = new PGPPublicKeyRingCollection(Collections.singletonList(gen.generatePublicKeyRing()));
					secColl = new PGPSecretKeyRingCollection(Collections.singletonList(gen.generateSecretKeyRing()));

					FileOutputStream pubFOS = new FileOutputStream(gpgPublicKeyCollectionFile);
					pubColl.encode(pubFOS);
					pubFOS.close();

					FileOutputStream secFOS = new FileOutputStream(gpgSecretKeyCollectionFile);
					secColl.encode(secFOS);
					secFOS.close();
				}
			} else {
				System.err.println("Invalid GPG keyring collection files defined");
				System.err.println(USAGE);
				System.exit(1);
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.err.println(USAGE);
			System.exit(1);
		}

		FileOutputStream publicWriter = null;
		FileOutputStream privateWriter = null;
		try {
			publicWriter = new FileOutputStream(publicFile);
			privateWriter = new FileOutputStream(privateFile);

			MessageDigest sha256 = MessageDigest.getInstance("SHA-256");

			PGPSecretKeyRing ring = (PGPSecretKeyRing) secColl.getKeyRings().next();
			PGPSecretKey secretKey = ring.getSecretKey();

			Calendar c = Calendar.getInstance();
			c.setTimeZone(TimeZone.getTimeZone("UTC"));
			c.setTime(firstDrawDate);
			c.set(Calendar.HOUR, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0);

			for (int i = 0; i < numTickets; i++) {
				String drawDate = DF.format(c.getTime());

				Set<Integer> winningNumbers = pickNumbers(rand);
				List<Integer> sortedWinningNumbers = new ArrayList<Integer>(winningNumbers);
				Collections.sort(sortedWinningNumbers);

				byte[] secret = new byte[SECRET_SIZE];
				rand.nextBytes(secret);

				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				baos.write(secret);
				baos.write(drawDate.getBytes(Charsets.UTF_8));
				for (Integer number : sortedWinningNumbers) {
					baos.write((byte) number.intValue());
				}

				sha256.reset();

				byte[] verifier = baos.toByteArray();
				baos.close();

				for (int j = 0; j < Constants.HASH_ITERATIONS; j++) {
					verifier = sha256.digest(verifier);
				}

				byte[] signature = GPGUtils.signMessage(verifier, secretKey, passwd, "SHA256", rand);
				String signatureStr = new String(signature, Charsets.UTF_8);

				JsonObject publicJson = new JsonObject();
				publicJson.add("drawDate", new JsonPrimitive(drawDate));
				publicJson.add("signature", new JsonPrimitive(signatureStr));

				publicWriter.write(publicJson.toString().getBytes(Charsets.UTF_8));
				publicWriter.write("\n".getBytes(Charsets.UTF_8));

				JsonObject privateJson = new JsonObject();

				JsonArray numbersArray = new JsonArray();
				for (Integer num : sortedWinningNumbers) {
					numbersArray.add(new JsonPrimitive(num));
				}

				privateJson.add("drawDate", new JsonPrimitive(drawDate));
				privateJson.add("numbers", numbersArray);
				privateJson.add("secret", new JsonPrimitive(Base64.encodeBase64String(secret)));

				privateWriter.write(privateJson.toString().getBytes(Charsets.UTF_8));
				privateWriter.write("\n".getBytes(Charsets.UTF_8));

				c.add(Calendar.DAY_OF_YEAR, daysBetweenDraws);
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.err.println(USAGE);
			System.exit(1);
		} finally {
			if (publicWriter != null) {
				try {
					publicWriter.close();
				} catch (IOException e) {
					System.out.println("Unable to close publicWriter: " + e.getMessage());
				}
			}

			if (privateWriter != null) {
				try {
					privateWriter.close();
				} catch (IOException e) {
					System.out.println("Unable to close privateWriter: " + e.getMessage());
				}
			}
		}
	}
}
