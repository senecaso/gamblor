/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.test;

import com.bitsofproof.supernode.api.AccountManager;
import com.bitsofproof.supernode.api.AddressConverter;
import com.bitsofproof.supernode.api.FileWallet;
import com.bitsofproof.supernode.api.JMSServerConnector;
import com.bitsofproof.supernode.api.Transaction;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.fusesource.stomp.jms.StompJmsConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.math.BigDecimal;
import java.security.Security;
import java.util.UUID;

public class ReturnBTC {
	private static final Logger logger = LoggerFactory.getLogger(ReturnBTC.class);

	private static final String USAGE = "ReturnBTC <path to wallet file>";

	private static final String ACCOUNT = "hotAccount";

	public static void main(String[] args) throws Exception {
		Security.addProvider(new BouncyCastleProvider());

		String walletFilePath = args[0];
		if (walletFilePath == null || walletFilePath.trim().isEmpty()) {
			System.err.println(USAGE);
			System.exit(1);
		}

		File walletFile = new File(walletFilePath);
		if (walletFile.isDirectory() || !walletFile.canWrite()) {
			System.err.println("wallet must be a writable file");
			System.err.println(USAGE);
			System.exit(1);
		}

		String brokerURI = "tcp://localhost:61613";
		String username = "admin";
		String password = "password";

		StompJmsConnectionFactory connectionFactory = new StompJmsConnectionFactory();
		connectionFactory.setBrokerURI(brokerURI);
		connectionFactory.setUsername(username);
		connectionFactory.setPassword(password);

		JMSServerConnector api = new JMSServerConnector();
		api.setConnectionFactory(connectionFactory);
		api.setClientId(UUID.randomUUID().toString());
		api.init();
		api.isProduction();

		int addressFlag = 111; // testnet

		FileWallet wallet = new FileWallet(walletFile.getAbsolutePath());
		if (!wallet.exists()) {
			wallet.init(password);
			wallet.persist();
		} else {
			wallet = FileWallet.read(walletFile.getAbsolutePath());
			wallet.sync(api, 200);
		}

		AccountManager mgr = wallet.getAccountManager(ACCOUNT);
		if (mgr != null) {
			String msg = "\nwallet:\n"
					+ "\tbalance: {}\n"
					+ "\tconfirmed: {}\n"
					+ "\tsending: {}\n"
					+ "\treceiving: {}\n";
			logger.info(msg, new Object[]{BigDecimal.valueOf(mgr.getBalance()).movePointLeft(8), BigDecimal.valueOf(mgr.getConfirmed()).movePointLeft(8), BigDecimal.valueOf(mgr.getSending()).movePointLeft(8), BigDecimal.valueOf(mgr.getReceiving()).movePointLeft(8)});

			long balance = mgr.getConfirmed() - 200000;

			if (balance > 0) {
				wallet.unlock(password);
				Transaction t = mgr.pay(AddressConverter.fromSatoshiStyle("mxNweGgVXfvmBKYNhftXVYyDVBTyjwxgQ8", addressFlag), balance);
				api.sendTransaction(t);
				logger.info("submitted txn {} with a balance of {}", new Object[] {t.getHash(), balance});
			} else {
				logger.info("nothing to send");
			}

			for (Transaction t : mgr.getTransactions()) {
				logger.info("txn: {}", t.getHash());
			}
		}

		api.destroy();
	}
}
