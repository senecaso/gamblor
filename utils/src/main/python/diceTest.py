import requests, json, time

s = requests.Session()

headers = {'content-type': 'application/json'}

# create an account
payload = {'returnAddress':'mxNweGgVXfvmBKYNhftXVYyDVBTyjwxgQ8'}
r = s.post('http://localhost:8080/bookie/accounts', data=json.dumps(payload), headers=headers)
depositAddress = r.json()['depositAddress']
accoundId = r.json()['accountId']
print 'Deposit funds to ' + depositAddress
time.sleep(120)

for x in range(0, 5000):
  # get a seed
  r = s.get("http://localhost:8080/bookie/seeds")
  seedHash = r.json()['hash']

  # place a bet
  payload = {'accountId':accoundId,'stake':0.01,'betName':64000,'clientSeed':'AAECAwQFBgcICQoLDA0ODw==','serverSeedHash':seedHash}
  r = s.post('http://localhost:8080/bones/bets', data=json.dumps(payload), headers=headers)
  print r.json()['payout']

# cash out
r = s.delete('http://localhost:8080/bookie/accounts/' + accoundId, stream=False)
print "Requested cash out"