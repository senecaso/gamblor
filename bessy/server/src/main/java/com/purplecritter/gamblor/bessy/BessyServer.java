/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bessy;

import com.google.common.collect.ImmutableMap;
import com.netflix.astyanax.AstyanaxContext;
import com.netflix.astyanax.Cluster;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.connectionpool.NodeDiscoveryType;
import com.netflix.astyanax.connectionpool.impl.ConnectionPoolConfigurationImpl;
import com.netflix.astyanax.connectionpool.impl.CountingConnectionPoolMonitor;
import com.netflix.astyanax.impl.AstyanaxConfigurationImpl;
import com.netflix.astyanax.thrift.ThriftFamilyFactory;
import com.purplecritter.gamblor.dice.Dice;
import com.purplecritter.gamblor.bookie.data.accounts.CassandraAccountManager;
import com.purplecritter.gamblor.bookie.data.events.CassandraEventManager;
import com.purplecritter.gamblor.bookie.data.CassandraSeedManager;
import com.purplecritter.gamblor.bookie.model.accounts.AccountManager;
import com.purplecritter.gamblor.bookie.model.events.EventManager;
import com.purplecritter.gamblor.bookie.model.seeds.SeedManager;
import com.purplecritter.gamblor.bookie.processor.BOPPaymentProcessor;
import com.purplecritter.gamblor.bookieapi.Bookie;
import com.purplecritter.gamblor.roulette.Roulette;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.restlet.Component;
import org.restlet.data.Protocol;

import java.io.File;
import java.security.Security;

public class BessyServer {
	private static final String USAGE = "BessyServer <walletFile>";

	private static final String CLUSTER = "cluster";
	private static final String KEYSPACE = "bookie";

	public static void main(String[] args) throws Exception {
		Security.addProvider(new BouncyCastleProvider());

		String walletFilePath = args[0];
		if (walletFilePath == null || walletFilePath.trim().isEmpty()) {
			System.err.println(USAGE);
			System.exit(1);
		}

		File walletFile = new File(walletFilePath);
		if (walletFile.isDirectory()) {
			System.err.println("wallet must be a file");
			System.err.println(USAGE);
			System.exit(1);
		}

		AstyanaxContext<Cluster> context = new AstyanaxContext.Builder()
				.forCluster(CLUSTER)
				.withAstyanaxConfiguration(new AstyanaxConfigurationImpl()
						.setDiscoveryType(NodeDiscoveryType.RING_DESCRIBE)
				)
				.withConnectionPoolConfiguration(new ConnectionPoolConfigurationImpl("MyConnectionPool")
						.setPort(9160)
						.setMaxConnsPerHost(5)
						.setSeeds("127.0.0.1:9160")
				)
				.withConnectionPoolMonitor(new CountingConnectionPoolMonitor()).buildCluster(new ThriftFamilyFactory());

		context.start();

		Cluster cluster = context.getClient();
		Keyspace ks = cluster.getKeyspace(KEYSPACE);

		if (cluster.describeKeyspace(KEYSPACE) == null) {
			ks.createKeyspace(ImmutableMap.<String, Object>builder()
					.put("strategy_options", ImmutableMap.<String, Object>builder()
							.put("replication_factor", "1") // TODO: need a real replication factor
							.build())
					.put("strategy_class", "SimpleStrategy")
					.build()
			);
		}

		String brokerURI = "tcp://localhost:61613";
		String username = "admin";
		String password = "password";
		BOPPaymentProcessor processor = new BOPPaymentProcessor(brokerURI, username, password, walletFile, ks);

		AccountManager accountManager = new CassandraAccountManager(processor, ks);
		EventManager eventManager = new CassandraEventManager(ks, accountManager);
		SeedManager seedManager = new CassandraSeedManager(ks);

		Component component = new Component();
		component.getServers().add(Protocol.HTTP, 8080);
		component.getDefaultHost().attach("/bookie", new Bookie(processor, accountManager, eventManager, seedManager));
		component.getDefaultHost().attach("/dice", new Dice(processor, accountManager, eventManager, seedManager));
		component.getDefaultHost().attach("/roulette", new Roulette(processor, accountManager, eventManager, seedManager));
		component.start();
	}
}
