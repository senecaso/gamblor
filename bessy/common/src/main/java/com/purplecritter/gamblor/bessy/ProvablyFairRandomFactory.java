/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bessy;

import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ProvablyFairRandomFactory {
	private static final int MIN_SERVER_SEED = 16;
	private static final int MIN_CLIENT_SEED = 16;

	public static RandomGenerator get(byte[] serverSeed, byte[] clientSeed) {
		if (serverSeed == null || serverSeed.length < MIN_SERVER_SEED) {
			throw new IllegalArgumentException("serverSeed must be at least " + MIN_SERVER_SEED + " bytes");
		}

		if (clientSeed == null || clientSeed.length < MIN_CLIENT_SEED) {
			throw new IllegalArgumentException("clientSeed must be at least " + MIN_CLIENT_SEED + " bytes");
		}

		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			digest.update(serverSeed);
			digest.update(clientSeed);
			byte[] seedBytes = digest.digest();

			IntBuffer intBuffer = ByteBuffer.wrap(seedBytes).asIntBuffer();
			int[] seedData = new int[intBuffer.remaining()];
			intBuffer.get(seedData);

			return new MersenneTwister(seedData);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Unable to find SHA-256.  This is a big problem!", e);
		}
	}
}
