/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bessy;

import com.purplecritter.gamblor.bookie.model.accounts.AccountManager;
import com.purplecritter.gamblor.bookie.model.events.EventManager;
import com.purplecritter.gamblor.bookie.model.seeds.SeedManager;
import com.purplecritter.gamblor.bookie.processor.PaymentProcessor;
import org.restlet.Application;

public abstract class AbstractApplication extends Application {
	private final PaymentProcessor processor;

	private final AccountManager accountManager;
	private final EventManager eventManager;
	private final SeedManager seedManager;

	public AbstractApplication(PaymentProcessor processor, AccountManager accountManager, EventManager eventManager, SeedManager seedManager) {
		if (processor == null) {
			throw new IllegalArgumentException("processor is required");
		}

		if (accountManager == null) {
			throw new IllegalArgumentException("accountManager is required");
		}

		if (eventManager == null) {
			throw new IllegalArgumentException("eventManager is required");
		}

		if (seedManager == null) {
			throw new IllegalArgumentException("seedManager is required");
		}

		this.processor = processor;
		this.accountManager = accountManager;
		this.eventManager = eventManager;
		this.seedManager = seedManager;
	}

	public PaymentProcessor getProcessor() {
		return processor;
	}

	public AccountManager getAccountManager() {
		return accountManager;
	}

	public EventManager getEventManager() {
		return eventManager;
	}

	public SeedManager getSeedManager() {
		return seedManager;
	}
}
