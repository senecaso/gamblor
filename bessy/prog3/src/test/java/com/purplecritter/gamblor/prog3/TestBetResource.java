/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.prog3;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.purplecritter.gamblor.bookie.model.accounts.Account;
import com.purplecritter.gamblor.bookie.model.seeds.Seed;
import junit.framework.Assert;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.junit.Test;

import java.math.BigDecimal;

public class TestBetResource extends BaseRestTestCase {
	@Test
	public void testBetting() throws Exception {
		// get a server seed
		Seed seed = seedManager.create();

		// create an account
		Account account = accountManager.create("myReturnAddress");

		// deposit some coins into the appropriate address
		processor.deposit(account.getDepositAddress(), "myTxn1", BigDecimal.ONE);

		// create a client side "seed"
		String clientSeed = Base64.encodeBase64String(new byte[] {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F});

		// place a bet
		JsonObject bet = new JsonObject();
		bet.add("accountId", new JsonPrimitive(account.getId().toString()));
		bet.add("clientSeed", new JsonPrimitive(clientSeed));
		bet.add("serverSeedHash", new JsonPrimitive(Base64.encodeBase64String(seed.getHash().asBytes())));

		JsonArray bets = new JsonArray();

		JsonArray betDetails = new JsonArray();
		betDetails.add(new JsonPrimitive(32));
		betDetails.add(new JsonPrimitive(0.25));
		bets.add(betDetails);

		betDetails = new JsonArray();
		betDetails.add(new JsonPrimitive(33));
		betDetails.add(new JsonPrimitive(0.25));
		bets.add(betDetails);

		betDetails = new JsonArray();
		betDetails.add(new JsonPrimitive(35));
		betDetails.add(new JsonPrimitive(0.25));
		bets.add(betDetails);

		betDetails = new JsonArray();
		betDetails.add(new JsonPrimitive(36));
		betDetails.add(new JsonPrimitive(0.25));
		bets.add(betDetails);

		bet.add("bets", bets);

		HttpPost post = new HttpPost(URL + "/bets");
		post.setEntity(new StringEntity(bet.toString(), ContentType.APPLICATION_JSON));
		JsonObject betResponse = getResponse(post);

		Assert.assertEquals(clientSeed, betResponse.get("clientSeed").getAsString());
		Assert.assertEquals(Base64.encodeBase64String(seed.getHash().asBytes()), betResponse.get("serverSeedHash").getAsString());
		Assert.assertTrue(betResponse.get("serverSeed") != null);
		Assert.assertTrue(betResponse.get("payout").getAsBigDecimal().compareTo(BigDecimal.ZERO) >= 0);
		Assert.assertTrue(betResponse.get("winningNumber").getAsInt() >= 0 && betResponse.get("winningNumber").getAsInt() <= 37);
	}
}
