/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.prog3;

import com.google.common.hash.HashCodes;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import com.purplecritter.gamblor.bessy.BaseResource;
import com.purplecritter.gamblor.bessy.ProvablyFairRandomFactory;
import com.purplecritter.gamblor.bookie.exceptions.AccountClosedException;
import com.purplecritter.gamblor.bookie.exceptions.EventClosedException;
import com.purplecritter.gamblor.bookie.exceptions.InsufficientFundsException;
import com.purplecritter.gamblor.bookie.exceptions.LoadException;
import com.purplecritter.gamblor.bookie.exceptions.PaymentException;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.exceptions.UnknownAccountException;
import com.purplecritter.gamblor.bookie.model.accounts.Account;
import com.purplecritter.gamblor.bookie.model.accounts.AccountId;
import com.purplecritter.gamblor.bookie.model.accounts.AccountManager;
import com.purplecritter.gamblor.bookie.model.bets.Bet;
import com.purplecritter.gamblor.bookie.model.bets.BetId;
import com.purplecritter.gamblor.bookie.model.events.Event;
import com.purplecritter.gamblor.bookie.model.events.EventManager;
import com.purplecritter.gamblor.bookie.model.seeds.Seed;
import com.purplecritter.gamblor.bookie.model.seeds.SeedManager;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.math3.random.RandomGenerator;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.EOFException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class BetResource extends BaseResource {
	private static final Logger logger = LoggerFactory.getLogger(BetResource.class);

	private static final String GAME = "prog3";

	private static final int MIN_NUMBER = 1;
	private static final int MAX_NUMBER = 27;
	private static final BigDecimal BASE_POT = BigDecimal.TEN;

	private static final int MAX_CLIENT_SEED_LENGTH = 32;
	private static final MathContext MC = new MathContext(8, RoundingMode.HALF_UP);

	private static final BigDecimal STAKE = BigDecimal.valueOf(0.01);

	private static final String ACCOUNT_ID = "accountId";
	private static final String CLIENT_SEED = "clientSeed";
	private static final String SERVER_SEED_HASH = "serverSeedHash";
	private static final String NUMBERS = "numbers";

	private static final String PAYOUT = "payout";
	private static final String SERVER_SEED = "serverSeed";

	private static final String WINNING_NUMBERS = "winningNumbers";

	private static final NumberFormat nf = new DecimalFormat("0.0000000");
	private static final ScheduledExecutorService stats = Executors.newScheduledThreadPool(1);
	private static volatile int betCount = 0;

	static {
		stats.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				double rate = betCount * 1.0 / 10.0;
				if (rate > 0.0) {
					logger.info("PROG3 processed {} bets/s", nf.format(rate));
				}

				betCount = 0;
			}
		}, 10000, 10000, TimeUnit.MILLISECONDS);
	}

	private static Event currentEvent;
	private static Object eventLock = new Object();

	private final JsonParser parser = new JsonParser();

	private final EventManager eventManager;
	private final AccountManager accountManager;
	private final SeedManager seedManager;

	public BetResource() {
		this.accountManager = getApplication().getAccountManager();
		this.eventManager = getApplication().getEventManager();
		this.seedManager = getApplication().getSeedManager();
	}

	private Event getEvent() throws StoreException, LoadException {
		synchronized (eventLock) {
			if (currentEvent == null) {
				List<Event> existingEvents = eventManager.get(GAME);
				for (Event existingEvent : existingEvents) {
					if (!existingEvent.isClosed()) {
						if (currentEvent != null) {
							logger.error("More than 1 current PROG3 event.  This means the pool will not be properly sized!");
						}

						currentEvent = existingEvent;
					}
				}
			} else if (currentEvent.isClosed()) {
				currentEvent = eventManager.create(GAME);
			}

			return currentEvent;
		}
	}

	private Set<Integer> getWinningNumbers(JsonObject outcome) {
		byte[] serverSeed = Base64.decodeBase64(outcome.get(SERVER_SEED).getAsString());
		byte[] clientSeed = Base64.decodeBase64(outcome.get(CLIENT_SEED).getAsString());

		RandomGenerator rand = ProvablyFairRandomFactory.get(serverSeed, clientSeed);

		Set<Integer> numbers = new HashSet<>();
		while (numbers.size() < 3) {
			numbers.add(rand.nextInt(MAX_NUMBER) + MIN_NUMBER);
		}

		return numbers;
	}

	private boolean isWinner(JsonObject outcome) {
		return isWinner(outcome, getWinningNumbers(outcome));
	}

	private boolean isWinner(JsonObject outcome, Set<Integer> winningNumbers) {
		Set<Integer> numbers = new HashSet<>();
		for (JsonElement el : outcome.get(NUMBERS).getAsJsonArray()) {
			numbers.add(el.getAsInt());
		}

		return winningNumbers.equals(numbers);
	}

	private BigDecimal calculatePayout(Event event) {
		Map<AccountId, List<BetId>> confirmedBets = new HashMap<>();
		for (Map.Entry<BetId, AccountId> entry : event.getBets().entrySet())  {
			List<BetId> bets = confirmedBets.get(entry.getValue());
			if (bets == null) {
				bets = new ArrayList<>();
				confirmedBets.put(entry.getValue(), bets);
			}

			bets.add(entry.getKey());
		}

		// remove all of the bets placed by any account which was deemed a cheater/double-spend
		Iterator<AccountId> itr = confirmedBets.keySet().iterator();
		while (itr.hasNext()) {
			AccountId accountId = itr.next();
			Account account = accountManager.get(accountId);
			if (account.isDoubleSpend()) {
				itr.remove();
			}
		}

		int numConfirmedBets = 0;
		for (List<BetId> betIds : confirmedBets.values()) {
			numConfirmedBets += betIds.size();
		}

		Map<BetId, AccountId> bets = event.getBets();
		int numWinners = 0;
		for (BetId unsettledBetId : event.getUnsettledBets()) {
			Bet unsettledBet = accountManager.get(bets.get(unsettledBetId)).getBets().get(unsettledBetId);
			JsonObject outcome = unsettledBet.getOutcome();
			if (isWinner(outcome)) {
				numWinners++;
			}
		}

		return BASE_POT.add(BigDecimal.valueOf(numConfirmedBets).multiply(STAKE, MC)).divide(BigDecimal.valueOf(numWinners), MC);
	}

	@Post
	public String placeBet(Representation entity) throws IOException {
		Event event = null;

		try {
			/*
			 * validate the incoming payload
			 */
			if (entity == null || !entity.getMediaType().isCompatible(MediaType.APPLICATION_JSON)) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(MediaType.APPLICATION_JSON + " content-type is required");
			}

			JsonElement el = parser.parse(entity.getReader());
			if (!el.isJsonObject()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult("invalid payload format");
			}

			JsonObject json = el.getAsJsonObject();

			JsonElement accountEl = json.get(ACCOUNT_ID);
			if (accountEl == null || !accountEl.isJsonPrimitive() || !accountEl.getAsJsonPrimitive().isString() || accountEl.getAsString().trim().isEmpty()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(ACCOUNT_ID + " is invalid");
			}

			JsonElement clientSeedEl = json.get(CLIENT_SEED);
			if (clientSeedEl == null || !clientSeedEl.isJsonPrimitive() || !clientSeedEl.getAsJsonPrimitive().isString() || clientSeedEl.getAsString().trim().isEmpty()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(CLIENT_SEED + " is invalid");
			}

			JsonElement serverSeedHashEl = json.get(SERVER_SEED_HASH);
			if (serverSeedHashEl == null || !serverSeedHashEl.isJsonPrimitive() || !serverSeedHashEl.getAsJsonPrimitive().isString() || serverSeedHashEl.getAsString().trim().isEmpty()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(SERVER_SEED_HASH + " is invalid");
			}

			JsonElement numbersEl = json.get(NUMBERS);
			if (numbersEl == null || !numbersEl.isJsonArray() || numbersEl.getAsJsonArray().size() != 3) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(NUMBERS + " is invalid");
			}

			/*
			 * validate the given values
			 */
			String accountIdStr = accountEl.getAsString();
			String clientSeedStr = clientSeedEl.getAsString();

			if (clientSeedStr.length() <= 0 || clientSeedStr.length() > MAX_CLIENT_SEED_LENGTH) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(CLIENT_SEED + " must be no more than " + MAX_CLIENT_SEED_LENGTH + " characters long");
			}

			Set<Integer> numbers = new HashSet<>();
			for (JsonElement numberEl : numbersEl.getAsJsonArray()) {
				if (!numberEl.isJsonPrimitive() || !numberEl.getAsJsonPrimitive().isNumber()) {
					setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
					return prepareErrorResult(NUMBERS + " is invalid");
				}

				int number = numberEl.getAsInt();
				if (number < MIN_NUMBER || number > MAX_NUMBER) {
					setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
					return prepareErrorResult(NUMBERS + " is invalid");
				}

				numbers.add(number);
			}

			if (numbers.size() != 3) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(NUMBERS + " is invalid");
			}

			try {
				/*
				 * process the bet
				 */

				Seed seed = seedManager.get(HashCodes.fromBytes(Base64.decodeBase64(serverSeedHashEl.getAsString())));
				if (seed == null) {
					setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
					return prepareErrorResult(SERVER_SEED_HASH + " is invalid");
				}

				try {
					byte[] clientSeed = Base64.decodeBase64(clientSeedStr);
					if (clientSeed == null || clientSeed.length <= 0) {
						setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
						return prepareErrorResult(CLIENT_SEED + " is invalid");
					}

					JsonObject outcome = new JsonObject();
					outcome.add(SERVER_SEED, new JsonPrimitive(Base64.encodeBase64String(seed.getSeed())));
					outcome.add(SERVER_SEED_HASH, new JsonPrimitive(Base64.encodeBase64String(seed.getHash().asBytes())));
					outcome.add(CLIENT_SEED, new JsonPrimitive(clientSeedStr));
					outcome.add(NUMBERS, numbersEl);

					event = getEvent();

					// TODO: make sure this is still the same event the user wanted to bet on

					AccountId accountId = new AccountId(accountIdStr);
					BetId betId = event.placeBet(accountId, STAKE, outcome);

					Set<Integer> winningNumbers = getWinningNumbers(outcome);

					BigDecimal payout = BigDecimal.ZERO;
					if (isWinner(outcome, winningNumbers)) {
						event.closeEvent();
						payout = calculatePayout(event);
					}

					event.settleBet(betId, payout);

					JsonObject response = new JsonObject();
					response.add(PAYOUT, new JsonPrimitive(payout));
					response.add(SERVER_SEED, new JsonPrimitive(Base64.encodeBase64String(seed.getSeed())));
					response.add(SERVER_SEED_HASH, new JsonPrimitive(Base64.encodeBase64String(seed.getHash().asBytes())));
					response.add(CLIENT_SEED, new JsonPrimitive(clientSeedStr));

					JsonArray winningNumbersEl = new JsonArray();
					for (Integer i : winningNumbers) {
						winningNumbersEl.add(new JsonPrimitive(i));
					}
					response.add(WINNING_NUMBERS, winningNumbersEl);

					betCount++;

					return response.toString();
				} finally {
					seedManager.remove(seed.getHash());
				}
			} catch (StoreException | LoadException e) {
				logger.error("Unable to connect to storage", e);

				setStatus(Status.SERVER_ERROR_SERVICE_UNAVAILABLE);
				return prepareErrorResult("Unable to connect to storage");
			} catch (InsufficientFundsException e) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult("Insufficient funds");
			} catch (AccountClosedException e) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult("Account closed");
			} catch (UnknownAccountException e) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult("Unknown account");
			} catch (EventClosedException e) {
				logger.warn("Attempted to place a bet on an event that was already closed: {} {}", event.getId());

				setStatus(Status.SERVER_ERROR_INTERNAL);
				return prepareErrorResult("Event closed");
			} catch (PaymentException e) {
				logger.error("Failed to complete a cash-out after settling the final bet.  Manual cash-out will be required for account: {}", accountIdStr, e);
				setStatus(Status.SERVER_ERROR_INTERNAL);
				return prepareErrorResult("Failed to complete cash-out after settling final bet");
			}
		} catch (EOFException | JsonSyntaxException e) {
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return prepareErrorResult("invalid payload format");
		}
	}
}
