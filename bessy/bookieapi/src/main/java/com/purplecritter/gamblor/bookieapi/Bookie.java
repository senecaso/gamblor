/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookieapi;

import com.purplecritter.gamblor.bessy.AbstractApplication;
import com.purplecritter.gamblor.bookie.model.accounts.AccountManager;
import com.purplecritter.gamblor.bookie.model.events.EventManager;
import com.purplecritter.gamblor.bookie.model.seeds.SeedManager;
import com.purplecritter.gamblor.bookie.processor.PaymentProcessor;
import org.restlet.Restlet;
import org.restlet.routing.Router;

public class Bookie extends AbstractApplication {
	public Bookie(PaymentProcessor processor, AccountManager accountManager, EventManager eventManager, SeedManager seedManager) {
		super(processor, accountManager, eventManager, seedManager);
	}

	@Override
	public Restlet createInboundRoot() {
		Router router = new Router(getContext());

		router.attach("/accounts", AccountResource.class);
		router.attach("/accounts/{accountId}", AccountResource.class);

		router.attach("/events", EventResource.class);
		router.attach("/events/{eventId}", EventResource.class);

		router.attach("/events/{eventId}/bets", BetResource.class);
		router.attach("/events/{eventId}/bets/{betId}", BetResource.class);

		router.attach("/seeds", SeedResource.class);
		router.attach("/seeds/{hash}", SeedResource.class);

		return router;
	}
}
