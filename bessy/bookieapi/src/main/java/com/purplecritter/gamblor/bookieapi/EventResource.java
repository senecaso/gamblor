/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookieapi;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import com.purplecritter.gamblor.bessy.BaseResource;
import com.purplecritter.gamblor.bookie.exceptions.LoadException;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.model.events.Event;
import com.purplecritter.gamblor.bookie.model.events.EventId;
import com.purplecritter.gamblor.bookie.model.events.EventManager;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.Delete;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.EOFException;
import java.io.IOException;

public class EventResource extends BaseResource {
	private static final Logger logger = LoggerFactory.getLogger(EventResource.class);

	private static final String EVENT_ID = "eventId";
	private static final String GAME = "game";

	private final JsonParser parser = new JsonParser();
	private final EventManager eventManager;

	public EventResource() {
		this.eventManager = getApplication().getEventManager();
	}

	@Post
	public String createEvent(Representation entity) throws IOException {
		try {
			if (entity == null || !entity.getMediaType().isCompatible(MediaType.APPLICATION_JSON)) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult("invalid media type");
			}

			JsonElement el = parser.parse(entity.getReader());
			if (!el.isJsonObject()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult("invalid payload format");
			}

			JsonObject json = el.getAsJsonObject();
			JsonElement gameEl = json.get(GAME);
			if (gameEl == null || !gameEl.isJsonPrimitive() || !gameEl.getAsJsonPrimitive().isString() || gameEl.getAsString().trim().isEmpty()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(GAME + " is invalid");
			}

			Event event = eventManager.create(gameEl.getAsString());

			JsonObject response = new JsonObject();
			response.add(EVENT_ID, new JsonPrimitive(event.getId().toString()));

			return response.toString();
		} catch (StoreException e) {
			logger.error("Failed to create event", e);

			setStatus(Status.SERVER_ERROR_INTERNAL);
			return prepareErrorResult("Unable to create event at this time");
		} catch (EOFException | JsonSyntaxException e) {
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return prepareErrorResult("invalid payload format");
		}
	}

	@Delete
	public String closeEvent() {
		try {
			String eventIdStr = (String) getRequest().getAttributes().get(EVENT_ID);
			if (eventIdStr == null || eventIdStr.trim().isEmpty()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				prepareErrorResult("invalid eventId");
			}

			EventId eventId = new EventId(eventIdStr);
			logger.debug("request to close event {}", eventId);

			Event event = eventManager.get(eventId);
			if (event == null) {
				setStatus(Status.CLIENT_ERROR_NOT_FOUND);
				return prepareErrorResult(EVENT_ID + " is invalid");
			}

			event.closeEvent();

			setStatus(Status.SUCCESS_NO_CONTENT);
			return null;
		} catch (StoreException | LoadException e) {
			logger.error("Failed to close event", e);

			setStatus(Status.SERVER_ERROR_INTERNAL);
			return prepareErrorResult("Unable to close event at this time");
		}
	}
}
