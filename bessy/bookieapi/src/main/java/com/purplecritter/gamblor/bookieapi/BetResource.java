/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookieapi;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import com.purplecritter.gamblor.bessy.BaseResource;
import com.purplecritter.gamblor.bookie.exceptions.AccountClosedException;
import com.purplecritter.gamblor.bookie.exceptions.EventClosedException;
import com.purplecritter.gamblor.bookie.exceptions.InsufficientFundsException;
import com.purplecritter.gamblor.bookie.exceptions.LoadException;
import com.purplecritter.gamblor.bookie.exceptions.PaymentException;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.exceptions.UnknownAccountException;
import com.purplecritter.gamblor.bookie.model.accounts.AccountId;
import com.purplecritter.gamblor.bookie.model.bets.BetId;
import com.purplecritter.gamblor.bookie.model.events.Event;
import com.purplecritter.gamblor.bookie.model.events.EventId;
import com.purplecritter.gamblor.bookie.model.events.EventManager;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.EOFException;
import java.io.IOException;
import java.math.BigDecimal;

public class BetResource extends BaseResource {
	private static final Logger logger = LoggerFactory.getLogger(BetResource.class);

	private static final String EVENT_ID = "eventId";
	private static final String BET_ID = "betId";
	private static final String ACCOUNT_ID = "accountId";
	private static final String STAKE = "stake";
	private static final String OUTCOME = "outcome";
	private static final String PAYOUT = "payout";

	private final EventManager eventManager;

	private final JsonParser parser = new JsonParser();

	public BetResource() {
		this.eventManager = getApplication().getEventManager();
	}

	@Post
	public String placeBet(Representation entity) throws IOException {
		logger.debug("request to placeBet");

		try {
			if (entity == null || !entity.getMediaType().isCompatible(MediaType.APPLICATION_JSON)) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult("invalid media type");
			}

			/*
			 * validate the incoming parameters and payload
			 */
			String eid = (String) getRequest().getAttributes().get(EVENT_ID);
			if (eid == null || eid.trim().isEmpty()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(EVENT_ID + " is required");
			}

			if (!entity.getMediaType().isCompatible(MediaType.APPLICATION_JSON)) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(MediaType.APPLICATION_JSON + " content-type is required");
			}

			JsonElement el = parser.parse(entity.getReader());
			if (!el.isJsonObject()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult("invalid payload format");
			}

			JsonObject json = el.getAsJsonObject();
			JsonElement accountEl = json.get(ACCOUNT_ID);
			if (accountEl == null || !accountEl.isJsonPrimitive() || !accountEl.getAsJsonPrimitive().isString() || accountEl.getAsString().trim().isEmpty()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(ACCOUNT_ID + " is invalid");
			}

			JsonElement stakeEl = json.get(STAKE);
			if (stakeEl == null || !stakeEl.isJsonPrimitive() || !stakeEl.getAsJsonPrimitive().isNumber()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(STAKE + " is invalid");
			}

			JsonElement outcomeEl = json.get(OUTCOME);
			if (outcomeEl == null || !outcomeEl.isJsonObject()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(OUTCOME + " is invalid");
			}

			/*
			 * extract parameter and payload values
			 */
			EventId eventId = new EventId(eid);
			AccountId accountId = new AccountId(accountEl.getAsString());
			BigDecimal stake = stakeEl.getAsBigDecimal();
			JsonObject outcome = outcomeEl.getAsJsonObject();

			logger.debug("request to placeBet: {} {} {}", new Object[] {eventId, accountId, stake});

			/*
			 * place the bet
			 */

			Event event = eventManager.get(eventId);
			if (event == null) {
				setStatus(Status.CLIENT_ERROR_NOT_FOUND);
				return prepareErrorResult(EVENT_ID + " is invalid");
			}

			BetId betId = event.placeBet(accountId, stake, outcome);

			JsonObject response = new JsonObject();
			response.add(BET_ID, new JsonPrimitive(betId.toString()));

			return response.toString();
		} catch (EventClosedException | InsufficientFundsException | AccountClosedException e) {
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return prepareErrorResult(e.getMessage());
		} catch (UnknownAccountException e) {
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return prepareErrorResult(ACCOUNT_ID + " is invalid");
		} catch (LoadException | StoreException e) {
			logger.error("Failed to place bet", e);

			setStatus(Status.SERVER_ERROR_INTERNAL);
			return prepareErrorResult("Unable to place bet at this time");
		} catch (EOFException | JsonSyntaxException e) {
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return prepareErrorResult("invalid payload format");
		}
	}

	@Put
	public String settleBet(Representation entity) throws IOException {
		logger.debug("request to settleBet");

		/*
		 * validate the incoming parameters and payload
		 */

		String eid = (String) getRequest().getAttributes().get(EVENT_ID);
		if (eid == null || eid.trim().isEmpty()) {
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return prepareErrorResult(EVENT_ID + " is required");
		}

		String bId = (String) getRequest().getAttributes().get(BET_ID);
		if (bId == null || bId.trim().isEmpty()) {
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return prepareErrorResult(BET_ID + " is required");
		}

		if (!entity.getMediaType().isCompatible(MediaType.APPLICATION_JSON)) {
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return prepareErrorResult(MediaType.APPLICATION_JSON + " content-type is required");
		}

		JsonElement el = parser.parse(entity.getReader());
		if (!el.isJsonObject()) {
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return prepareErrorResult("invalid payload format");
		}

		JsonObject json = el.getAsJsonObject();
		JsonElement payoutEl = json.get(PAYOUT);
		if (payoutEl == null || !payoutEl.isJsonPrimitive() || !payoutEl.getAsJsonPrimitive().isNumber()) {
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return prepareErrorResult(PAYOUT + " is invalid");
		}

		/*
		 * extract parameter and payload values
		 */

		try {
			EventId eventId = new EventId(eid);
			BetId betId = new BetId(bId);
			BigDecimal payout = payoutEl.getAsBigDecimal();

			logger.debug("request to settleBet: {} {} {}", new Object[] {eventId, betId, payout});

			Event event = eventManager.get(eventId);
			if (event == null) {
				setStatus(Status.CLIENT_ERROR_NOT_FOUND);
				return prepareErrorResult(EVENT_ID + " is invalid");
			}

			event.settleBet(betId, payout);

			setStatus(Status.SUCCESS_NO_CONTENT);
			return null;
		} catch (LoadException | StoreException | PaymentException e) {
			logger.error("Failed to settle bet", e);

			setStatus(Status.SERVER_ERROR_INTERNAL);
			return prepareErrorResult("Unable to settle bet at this time");
		}
	}
}
