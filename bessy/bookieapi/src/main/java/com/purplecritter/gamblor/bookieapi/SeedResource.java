/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookieapi;

import com.google.common.base.Charsets;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashCodes;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.purplecritter.gamblor.bessy.BaseResource;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.model.seeds.Seed;
import com.purplecritter.gamblor.bookie.model.seeds.SeedManager;
import org.apache.commons.codec.binary.Base64;
import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class SeedResource extends BaseResource {
	private static final Logger logger = LoggerFactory.getLogger(SeedResource.class);

	private static final String HASH = "hash";
	private static final String SEED = "seed";

	private final SeedManager seedManager;

	public SeedResource() {
		seedManager = getApplication().getSeedManager();
	}

	@Get
	public String getSeed() {
		String hash = (String) getRequest().getAttributes().get(HASH);

		try {
			Seed seed;
			if (hash == null) {
				// create a new seed
				seed = seedManager.create();
			} else {
				// fetch an existing one
				byte[] hashBytes = Base64.decodeBase64(URLDecoder.decode(hash, Charsets.UTF_8.name()));
				if (hashBytes == null || hashBytes.length != Seed.SEED_LEN) {
					setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
					return prepareErrorResult("Invalid hash");
				}

				HashCode hashCode = HashCodes.fromBytes(hashBytes);
				seed = seedManager.get(hashCode);
			}

			if (seed == null) {
				setStatus(Status.CLIENT_ERROR_NOT_FOUND);
				return prepareErrorResult(HASH + " not found");
			} else {
				JsonObject json = new JsonObject();
				json.add(HASH, new JsonPrimitive(Base64.encodeBase64String(seed.getHash().asBytes())));
				json.add(SEED, new JsonPrimitive(Base64.encodeBase64String(seed.getSeed())));

				return json.toString();
			}
		} catch (StoreException e) {
			logger.error("Failed to store seed to storage", e);

			setStatus(Status.SERVER_ERROR_SERVICE_UNAVAILABLE);
			return prepareErrorResult("Unable to connect to storage");
		} catch (UnsupportedEncodingException e) {
			logger.error("This can never happen.  It means we cant support UTF-8!", e);

			setStatus(Status.SERVER_ERROR_INTERNAL);
			return prepareErrorResult("Failed to process.  Check server logs for details");
		}
	}
}
