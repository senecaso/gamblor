/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookieapi;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import com.purplecritter.gamblor.bessy.BaseResource;
import com.purplecritter.gamblor.bookie.exceptions.PaymentException;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.model.accounts.Account;
import com.purplecritter.gamblor.bookie.model.accounts.AccountId;
import com.purplecritter.gamblor.bookie.model.accounts.AccountManager;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.EOFException;
import java.io.IOException;

public class AccountResource extends BaseResource {
	private static final Logger logger = LoggerFactory.getLogger(AccountResource.class);

	private static final String ACCOUNT_ID = "accountId";
	private static final String BALANCE = "balance";
	private static final String IS_CONFIRMED = "isConfirmed";
	private static final String RETURN_ADDRESS = "returnAddress";
	private static final String DEPOSIT_ADDRESS = "depositAddress";

	private final JsonParser parser = new JsonParser();
	private final AccountManager accountManager;

	public AccountResource() {
		this.accountManager = getApplication().getAccountManager();
	}

	@Post
	public String createAccount(Representation entity) throws IOException {
		try {
			if (entity == null || !entity.getMediaType().isCompatible(MediaType.APPLICATION_JSON)) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult("invalid media type");
			}

			JsonElement el = parser.parse(entity.getReader());
			if (!el.isJsonObject()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult("invalid payload format");
			}

			JsonObject json = el.getAsJsonObject();
			JsonElement returnAddressEl = json.get(RETURN_ADDRESS);
			if (returnAddressEl == null || !returnAddressEl.isJsonPrimitive() || !returnAddressEl.getAsJsonPrimitive().isString() || returnAddressEl.getAsString().trim().isEmpty()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(RETURN_ADDRESS + " is invalid");
			}

			/*
			 * extract parameter and payload values
			 */
			String returnAddress = returnAddressEl.getAsString();

			if (false) { // TODO: validate the returnAddress
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult("The given return address is invalid");
			}

			Account account = accountManager.create(returnAddress);

			JsonObject response = new JsonObject();
			response.add(ACCOUNT_ID, new JsonPrimitive(account.getId().toString()));
			response.add(DEPOSIT_ADDRESS, new JsonPrimitive(account.getDepositAddress()));

			return response.toString();
		} catch (StoreException e) {
			logger.warn("Unable to create account", e);
			setStatus(Status.SERVER_ERROR_INTERNAL);
			return prepareErrorResult("Unable to create account right now");
		} catch (EOFException | JsonSyntaxException e) {
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return prepareErrorResult("invalid payload format");
		}
	}

	@Get
	public String getAccountDetails() {
		boolean isBalanceRequest = getQuery().getFirst(BALANCE) != null;
		boolean isConfirmationRequest = getQuery().getFirst(IS_CONFIRMED) != null;
		if (!isBalanceRequest && !isConfirmationRequest) {
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return prepareErrorResult("no account property specified");
		}

		AccountId accountId = new AccountId((String) getRequest().getAttributes().get(ACCOUNT_ID));
		Account account = accountManager.get(accountId);
		if (account == null) {
			setStatus(Status.CLIENT_ERROR_NOT_FOUND);
			return prepareErrorResult("No account with that id found");
		}

		JsonObject json = new JsonObject();

		if (isBalanceRequest) {
			json.add(BALANCE, new JsonPrimitive(account.getBalance()));
		}

		if (isConfirmationRequest) {
			json.add(IS_CONFIRMED, new JsonPrimitive(account.isConfirmed()));
		}

		return json.toString();
	}

	@Delete
	public String cashOut() {
		try {
			AccountId accountId = new AccountId((String) getRequest().getAttributes().get(ACCOUNT_ID));
			logger.info("request to cash out {}", accountId);

			Account account = accountManager.get(accountId);
			if (account == null) {
				setStatus(Status.CLIENT_ERROR_NOT_FOUND);
				return prepareErrorResult("No account with that id found");
			}

			account.cashOut();

			setStatus(Status.SUCCESS_NO_CONTENT);
			return null;
		} catch (StoreException | PaymentException e) {
			logger.error("Failed to cashOut", e);

			setStatus(Status.SERVER_ERROR_INTERNAL);
			return prepareErrorResult("Unable to process cashOut at this time");
		}
	}
}
