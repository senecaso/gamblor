/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookieapi;

import com.google.gson.JsonObject;
import junit.framework.Assert;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.junit.Test;

import java.math.BigDecimal;

public class TestAccountResource extends BaseRestTestCase {
	@Test
	public void testAccountCreation() throws Exception {
		HttpPost post = new HttpPost(URL + "/accounts");
		post.setEntity(new StringEntity("{\"returnAddress\":\"myReturnAddress\"}", ContentType.APPLICATION_JSON));
		JsonObject json = getResponse(post);

		Assert.assertTrue(json.get("accountId").getAsString().length() > 0);
		Assert.assertEquals("FAKEADDRESS", json.get("depositAddress").getAsString());
	}

	@Test
	public void testAccountDetails() throws Exception {
		// create an account
		HttpPost post = new HttpPost(URL + "/accounts");
		post.setEntity(new StringEntity("{\"returnAddress\":\"myReturnAddress\"}", ContentType.APPLICATION_JSON));
		JsonObject json = getResponse(post);

		String accountId = json.get("accountId").getAsString();
		String depositAddress = json.get("depositAddress").getAsString();
		Assert.assertTrue(accountId.length() > 0);
		Assert.assertEquals("FAKEADDRESS", depositAddress);

		// get its details
		HttpGet get = new HttpGet(URL + "/accounts/" + accountId + "?balance");
		json = getResponse(get);
		Assert.assertEquals(BigDecimal.ZERO, json.get("balance").getAsBigDecimal());

		get = new HttpGet(URL + "/accounts/" + accountId + "?isConfirmed");
		json = getResponse(get);
		Assert.assertTrue(json.get("isConfirmed").getAsBoolean());

		// make a deposit to see if the balance increases
		processor.deposit(depositAddress, "myTxn1", BigDecimal.ONE);

		get = new HttpGet(URL + "/accounts/" + accountId + "?balance");
		json = getResponse(get);
		Assert.assertEquals(BigDecimal.ONE, json.get("balance").getAsBigDecimal());

		get = new HttpGet(URL + "/accounts/" + accountId + "?isConfirmed");
		json = getResponse(get);
		Assert.assertFalse(json.get("isConfirmed").getAsBoolean());

		// confirm the deposit and check the account details again
		processor.setConfirmations("myTxn1", 1);

		get = new HttpGet(URL + "/accounts/" + accountId + "?isConfirmed");
		json = getResponse(get);
		Assert.assertTrue(json.get("isConfirmed").getAsBoolean());
	}

	@Test
	public void testCashout() throws Exception {
		// create an account
		HttpPost post = new HttpPost(URL + "/accounts");
		post.setEntity(new StringEntity("{\"returnAddress\":\"myReturnAddress\"}", ContentType.APPLICATION_JSON));
		JsonObject json = getResponse(post);

		String accountId = json.get("accountId").getAsString();
		String depositAddress = json.get("depositAddress").getAsString();
		Assert.assertTrue(accountId.length() > 0);
		Assert.assertEquals("FAKEADDRESS", depositAddress);

		// make a deposit and confirm it
		processor.deposit(depositAddress, "myTxn1", BigDecimal.ONE);
		processor.setConfirmations("myTxn1", 1);

		HttpDelete delete = new HttpDelete(URL + "/accounts/" + accountId);
		Assert.assertEquals(204, ignoreResponse(delete));

		// make sure the balance is back to 0
		HttpGet get = new HttpGet(URL + "/accounts/" + accountId + "?balance");
		json = getResponse(get);
		Assert.assertEquals(BigDecimal.ZERO, json.get("balance").getAsBigDecimal());
	}
}
