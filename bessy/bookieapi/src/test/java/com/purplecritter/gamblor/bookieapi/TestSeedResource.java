/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookieapi;

import com.google.common.base.Charsets;
import com.google.gson.JsonObject;
import junit.framework.Assert;
import org.apache.http.client.methods.HttpGet;
import org.junit.Test;

import java.net.URLEncoder;

public class TestSeedResource extends BaseRestTestCase {
	@Test
	public void testSeeds() throws Exception {
		// non-existent hash
		HttpGet get = new HttpGet(URL + "/seeds/" + URLEncoder.encode("7oh5HKGCA0GoTCgZ9VAkn6S3FRMFRVDpS9mDWfdo8QI=", Charsets.UTF_8.name()));
		FailureTuple tuple = getFailedResponse(get);
		Assert.assertEquals(404, tuple.statusCode);
		Assert.assertEquals("hash not found", tuple.json.get("details").getAsString());

		// create a seed
		get = new HttpGet(URL + "/seeds");
		JsonObject seed1 = getResponse(get);
		Assert.assertNotNull(seed1.get("hash").getAsString());
		Assert.assertNotNull(seed1.get("seed").getAsString());

		// create a second seed
		get = new HttpGet(URL + "/seeds");
		JsonObject seed2 = getResponse(get);
		Assert.assertNotNull(seed2.get("hash").getAsString());
		Assert.assertNotNull(seed2.get("seed").getAsString());
		Assert.assertFalse(seed1.get("hash").equals(seed2.get("hash")));
		Assert.assertFalse(seed1.get("seed").equals(seed2.get("seed")));

		// try to fetch the first seed
		get = new HttpGet(URL + "/seeds/" + URLEncoder.encode(seed1.get("hash").getAsString(), Charsets.UTF_8.name()));
		JsonObject fetched = getResponse(get);
		Assert.assertEquals(seed1, fetched);
	}

	@Test
	public void testInvalidHash() throws Exception {
		HttpGet get = new HttpGet(URL + "/seeds/weoirreytreuhuiferui");
		FailureTuple tuple = getFailedResponse(get);

		Assert.assertEquals(400, tuple.statusCode);
		Assert.assertEquals("Invalid hash", tuple.json.get("details").getAsString());

		get = new HttpGet(URL + "/seeds/嫌気");
		tuple = getFailedResponse(get);

		Assert.assertEquals(400, tuple.statusCode);
		Assert.assertEquals("Invalid hash", tuple.json.get("details").getAsString());
	}
}
