/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookieapi;

import com.google.gson.JsonObject;
import junit.framework.Assert;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.junit.Test;

import java.math.BigDecimal;

public class TestBetResource extends BaseRestTestCase {
	@Test
	public void testBetting() throws Exception {
		// create an event
		HttpPost post = new HttpPost(URL + "/events");
		post.setEntity(new StringEntity("{\"game\":\"test\"}", ContentType.APPLICATION_JSON));
		JsonObject json = getResponse(post);
		String eventId = json.get("eventId").getAsString();

		// create an account
		post = new HttpPost(URL + "/accounts");
		post.setEntity(new StringEntity("{\"returnAddress\":\"myReturnAddress\"}", ContentType.APPLICATION_JSON));
		json = getResponse(post);
		String accountId = json.get("accountId").getAsString();

		// deposit some funds into the account
		processor.deposit(json.get("depositAddress").getAsString(), "myTxn1", BigDecimal.TEN);
		processor.setConfirmations("myTxn1", 10);

		// place the bet
		post = new HttpPost(URL + "/events/" + eventId + "/bets");
		post.setEntity(new StringEntity("{\"accountId\":\"" + accountId + "\", \"stake\":0.50, \"outcome\":{}}", ContentType.APPLICATION_JSON));
		json = getResponse(post);
		String betId = json.get("betId").getAsString();
		Assert.assertTrue(betId.length() > 0);

		// make sure the available balance went down
		HttpGet get = new HttpGet(URL + "/accounts/" + accountId + "?balance");
		json = getResponse(get);
		Assert.assertEquals(new BigDecimal("9.50"), json.get("balance").getAsBigDecimal());

		// settle the bet as a win
		HttpPut put = new HttpPut(URL + "/events/" + eventId + "/bets/" + betId);
		put.setEntity(new StringEntity("{\"payout\":2.00}", ContentType.APPLICATION_JSON));
		Assert.assertEquals(204, ignoreResponse(put));

		// close the event
		HttpDelete delete = new HttpDelete(URL + "/events/" + eventId);
		Assert.assertEquals(204, ignoreResponse(delete));

		// make sure the account balance has increased
		get = new HttpGet(URL + "/accounts/" + accountId + "?balance");
		json = getResponse(get);
		Assert.assertEquals(new BigDecimal("11.50"), json.get("balance").getAsBigDecimal());

		// cash out
		delete = new HttpDelete(URL + "/accounts/" + accountId);
		Assert.assertEquals(204, ignoreResponse(delete));

		// make sure the balance is back to 0
		get = new HttpGet(URL + "/accounts/" + accountId + "?balance");
		json = getResponse(get);
		Assert.assertEquals(BigDecimal.ZERO, json.get("balance").getAsBigDecimal());
	}
}
