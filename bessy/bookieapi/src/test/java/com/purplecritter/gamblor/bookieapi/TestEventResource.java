/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookieapi;

import com.google.gson.JsonObject;
import junit.framework.Assert;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.junit.Test;

public class TestEventResource extends BaseRestTestCase {
	@Test
	public void testEventCreation() throws Exception {
		HttpPost post = new HttpPost(URL + "/events");
		post.setEntity(new StringEntity("{\"game\":\"test\"}", ContentType.APPLICATION_JSON));
		JsonObject json = getResponse(post);

		Assert.assertTrue(json.get("eventId").getAsString().length() > 0);
	}

	@Test
	public void testClosingEvent() throws Exception {
		HttpPost post = new HttpPost(URL + "/events");
		post.setEntity(new StringEntity("{\"game\":\"test\"}", ContentType.APPLICATION_JSON));
		JsonObject json = getResponse(post);

		String eventId = json.get("eventId").getAsString();
		Assert.assertTrue(eventId.length() > 0);

		HttpDelete delete = new HttpDelete(URL + "/events/" + eventId);
		Assert.assertEquals(204, ignoreResponse(delete));
	}
}
