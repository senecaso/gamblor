/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.roulette;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.purplecritter.gamblor.bookie.data.accounts.CassandraAccountManager;
import com.purplecritter.gamblor.bookie.data.events.CassandraEventManager;
import com.purplecritter.gamblor.bookie.data.CassandraSeedManager;
import com.purplecritter.gamblor.bookie.model.accounts.AccountManager;
import com.purplecritter.gamblor.bookie.model.events.EventManager;
import com.purplecritter.gamblor.bookie.model.seeds.SeedManager;
import com.purplecritter.gamblor.bookie.processor.MockPaymentProcessor;
import com.purplecritter.gamblor.test.BaseCassandraTest;
import junit.framework.Assert;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.restlet.Component;
import org.restlet.data.Protocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseRestTestCase extends BaseCassandraTest {
	private static final Logger logger = LoggerFactory.getLogger(BaseRestTestCase.class);

	protected static final String URL = "http://localhost:8080";

	protected static MockPaymentProcessor processor;
	private static Component component;
	protected static HttpClient httpclient;
	protected static SeedManager seedManager;
	protected static AccountManager accountManager;

	JsonParser parser = new JsonParser();

	public static class FailureTuple {
		public final int statusCode;
		public final JsonObject json;

		public FailureTuple(int statusCode, JsonObject response) {
			this.statusCode = statusCode;
			this.json = response;
		}
	}

	@BeforeClass
	public static void beforeClass() throws Exception {
		processor = new MockPaymentProcessor();
		accountManager = new CassandraAccountManager(processor, BaseCassandraTest.keyspace);
		EventManager eventManager = new CassandraEventManager(BaseCassandraTest.keyspace, accountManager);
		seedManager = new CassandraSeedManager(BaseCassandraTest.keyspace);

		logger.info("Starting REST server ...");
		component = new Component();
		component.getServers().add(Protocol.HTTP, 8080);
		component.getDefaultHost().attach("", new Roulette(processor, accountManager, eventManager, seedManager));
		component.start();

		httpclient = new DefaultHttpClient();
	}

	@AfterClass
	public static void afterClass() throws Exception {
		component.stop();
		httpclient.getConnectionManager().shutdown();
	}

	protected JsonObject getResponse(HttpRequestBase request) throws Exception {
		HttpResponse response = httpclient.execute(request);
		String body = EntityUtils.toString(response.getEntity());
		logger.info("json: {}", body);

		Assert.assertTrue(response.getStatusLine().getStatusCode() == 200);

		return (JsonObject) parser.parse(body);
	}

	protected int ignoreResponse(HttpRequestBase request) throws Exception {
		HttpResponse response = httpclient.execute(request);
		EntityUtils.consume(response.getEntity());
		Assert.assertTrue(response.getStatusLine().getStatusCode() == 200 || response.getStatusLine().getStatusCode() == 204);

		return response.getStatusLine().getStatusCode();
	}

	protected FailureTuple getFailedResponse(HttpRequestBase request) throws Exception {
		HttpResponse response = httpclient.execute(request);
		String body = EntityUtils.toString(response.getEntity());
		logger.info("json: {}", body);

		Assert.assertFalse(response.getStatusLine().getStatusCode() == 200);

		return new FailureTuple(response.getStatusLine().getStatusCode(), (JsonObject) parser.parse(body));
	}
}
