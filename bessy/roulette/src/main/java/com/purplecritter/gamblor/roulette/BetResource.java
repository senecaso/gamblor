/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.roulette;

import com.google.common.hash.HashCodes;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import com.purplecritter.gamblor.bessy.BaseResource;
import com.purplecritter.gamblor.bessy.ProvablyFairRandomFactory;
import com.purplecritter.gamblor.bookie.exceptions.AccountClosedException;
import com.purplecritter.gamblor.bookie.exceptions.EventClosedException;
import com.purplecritter.gamblor.bookie.exceptions.InsufficientFundsException;
import com.purplecritter.gamblor.bookie.exceptions.LoadException;
import com.purplecritter.gamblor.bookie.exceptions.PaymentException;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.exceptions.UnknownAccountException;
import com.purplecritter.gamblor.bookie.model.accounts.AccountId;
import com.purplecritter.gamblor.bookie.model.bets.BetId;
import com.purplecritter.gamblor.bookie.model.events.Event;
import com.purplecritter.gamblor.bookie.model.events.EventManager;
import com.purplecritter.gamblor.bookie.model.seeds.Seed;
import com.purplecritter.gamblor.bookie.model.seeds.SeedManager;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.math3.random.RandomGenerator;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.EOFException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class BetResource extends BaseResource {
	private static final Logger logger = LoggerFactory.getLogger(BetResource.class);

	private static final String GAME = "roulette";

	private static final int MAX_CLIENT_SEED_LENGTH = 32;
	private static final int MAX_NUMBER = 37;
	private static final MathContext MC = new MathContext(8, RoundingMode.HALF_UP);

	private static final String ACCOUNT_ID = "accountId";
	private static final String CLIENT_SEED = "clientSeed";
	private static final String SERVER_SEED_HASH = "serverSeedHash";
	private static final String BETS = "bets";

	private static final String PAYOUT = "payout";
	private static final String SERVER_SEED = "serverSeed";

	private static final String WINNING_NUMBER = "winningNumber";

	private static final NumberFormat nf = new DecimalFormat("0.0000000");
	private static final ScheduledExecutorService stats = Executors.newScheduledThreadPool(1);
	private static volatile int betCount = 0;

	static {
		stats.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				double rate = betCount * 1.0 / 10.0;
				if (rate > 0.0) {
					logger.info("ROULETTE processed {} bets/s", nf.format(rate));
				}

				betCount = 0;
			}
		}, 10000, 10000, TimeUnit.MILLISECONDS);
	}

	private static class BetPair {
		private final int number;
		private final BigDecimal stake;

		public BetPair(int number, BigDecimal stake) {
			if (number < 0 || number > MAX_NUMBER) {
				throw new IllegalArgumentException("number must be [0-" + MAX_NUMBER + "]");
			}

			if (stake == null || stake.compareTo(BigDecimal.ZERO) <= 0) {
				throw new IllegalArgumentException("stake must be > 0");
			}

			this.number = number;
			this.stake = stake;
		}
	}

	private final JsonParser parser = new JsonParser();

	private final EventManager eventManager;
	private final SeedManager seedManager;

	public BetResource() {
		this.eventManager = getApplication().getEventManager();
		this.seedManager = getApplication().getSeedManager();
	}

	private int getWinningNumber(byte[] serverSeed, byte[] clientSeed) {
		RandomGenerator rand = ProvablyFairRandomFactory.get(serverSeed, clientSeed);
		return rand.nextInt(MAX_NUMBER + 1);
	}

	private BigDecimal calculatePayout(List<BetPair> bets, int winningNumber) {
		BigDecimal payout = BigDecimal.ZERO;
		for (BetPair bet : bets) {
			if (bet.number == winningNumber) {
				payout = payout.add(bet.stake).add(BigDecimal.valueOf(35).multiply(bet.stake), MC);
			}
		}

		return payout;
	}

	@Post
	public String placeBet(Representation entity) throws IOException {
		Event event = null;

		try {
			/*
			 * validate the incoming payload
			 */
			if (entity == null || !entity.getMediaType().isCompatible(MediaType.APPLICATION_JSON)) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(MediaType.APPLICATION_JSON + " content-type is required");
			}

			JsonElement el = parser.parse(entity.getReader());
			if (!el.isJsonObject()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult("invalid payload format");
			}

			JsonObject json = el.getAsJsonObject();

			JsonElement accountEl = json.get(ACCOUNT_ID);
			if (accountEl == null || !accountEl.isJsonPrimitive() || !accountEl.getAsJsonPrimitive().isString() || accountEl.getAsString().trim().isEmpty()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(ACCOUNT_ID + " is invalid");
			}

			JsonElement clientSeedEl = json.get(CLIENT_SEED);
			if (clientSeedEl == null || !clientSeedEl.isJsonPrimitive() || !clientSeedEl.getAsJsonPrimitive().isString() || clientSeedEl.getAsString().trim().isEmpty()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(CLIENT_SEED + " is invalid");
			}

			JsonElement serverSeedHashEl = json.get(SERVER_SEED_HASH);
			if (serverSeedHashEl == null || !serverSeedHashEl.isJsonPrimitive() || !serverSeedHashEl.getAsJsonPrimitive().isString() || serverSeedHashEl.getAsString().trim().isEmpty()) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(SERVER_SEED_HASH + " is invalid");
			}

			JsonElement betsEl = json.get(BETS);
			if (betsEl == null || !betsEl.isJsonArray() || betsEl.getAsJsonArray().size() <= 0) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(BETS + " is invalid");
			}

			/*
			 * validate the given values
			 */
			String accountIdStr = accountEl.getAsString();
			String clientSeedStr = clientSeedEl.getAsString();

			if (clientSeedStr.length() <= 0 || clientSeedStr.length() > MAX_CLIENT_SEED_LENGTH) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult(CLIENT_SEED + " must be no more than " + MAX_CLIENT_SEED_LENGTH + " characters long");
			}

			List<BetPair> bets = new ArrayList<>();
			BigDecimal totalStake = BigDecimal.ZERO;
			for (JsonElement betEl : betsEl.getAsJsonArray()) {
				if (!betEl.isJsonArray()) {
					setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
					return prepareErrorResult(BETS + " is invalid");
				}

				JsonArray betArray = betEl.getAsJsonArray();
				if (!betArray.get(0).isJsonPrimitive() || !betArray.get(0).getAsJsonPrimitive().isNumber()
						|| !betArray.get(1).isJsonPrimitive() || !betArray.get(1).getAsJsonPrimitive().isNumber()) {
					setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
					return prepareErrorResult(BETS + " is invalid");

				}

				int number = betArray.get(0).getAsInt();
				BigDecimal stake = betArray.get(1).getAsBigDecimal();

				if (number < 0 || number > MAX_NUMBER || stake.compareTo(BigDecimal.ZERO) <= 0) {
					setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
					return prepareErrorResult(BETS + " is invalid");
				}

				bets.add(new BetPair(number, stake));
				totalStake = totalStake.add(stake);
			}

			try {
				/*
				 * process the bet
				 */

				Seed seed = seedManager.get(HashCodes.fromBytes(Base64.decodeBase64(serverSeedHashEl.getAsString())));
				if (seed == null) {
					setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
					return prepareErrorResult(SERVER_SEED_HASH + " is invalid");
				}

				try {
					byte[] clientSeed = Base64.decodeBase64(clientSeedStr);
					if (clientSeed == null || clientSeed.length <= 0) {
						setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
						return prepareErrorResult(CLIENT_SEED + " is invalid");
					}

					JsonObject outcome = new JsonObject();
					outcome.add(SERVER_SEED, new JsonPrimitive(Base64.encodeBase64String(seed.getSeed())));
					outcome.add(SERVER_SEED_HASH, new JsonPrimitive(Base64.encodeBase64String(seed.getHash().asBytes())));
					outcome.add(CLIENT_SEED, new JsonPrimitive(clientSeedStr));
					outcome.add(BETS, betsEl);

					event = eventManager.create(GAME);

					AccountId accountId = new AccountId(accountIdStr);
					BetId betId = event.placeBet(accountId, totalStake, outcome);
					event.closeEvent();

					int winningNumber = getWinningNumber(seed.getSeed(), clientSeed);
					BigDecimal payout = calculatePayout(bets, winningNumber);
					event.settleBet(betId, payout);

					JsonObject response = new JsonObject();
					response.add(PAYOUT, new JsonPrimitive(payout));
					response.add(SERVER_SEED, new JsonPrimitive(Base64.encodeBase64String(seed.getSeed())));
					response.add(SERVER_SEED_HASH, new JsonPrimitive(Base64.encodeBase64String(seed.getHash().asBytes())));
					response.add(CLIENT_SEED, new JsonPrimitive(clientSeedStr));
					response.add(WINNING_NUMBER, new JsonPrimitive(winningNumber));

					betCount++;

					return response.toString();
				} finally {
					seedManager.remove(seed.getHash());

					if (event != null && !event.isClosed()) {
						event.closeEvent();
					}
				}
			} catch (StoreException | LoadException e) {
				logger.error("Unable to connect to storage", e);

				setStatus(Status.SERVER_ERROR_SERVICE_UNAVAILABLE);
				return prepareErrorResult("Unable to connect to storage");
			} catch (InsufficientFundsException e) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult("Insufficient funds");
			} catch (AccountClosedException e) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult("Account closed");
			} catch (UnknownAccountException e) {
				setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
				return prepareErrorResult("Unknown account");
			} catch (EventClosedException e) {
				logger.warn("Attempted to place a bet on an event that was already closed: {} {}", event.getId());

				setStatus(Status.SERVER_ERROR_INTERNAL);
				return prepareErrorResult("Event closed");
			} catch (PaymentException e) {
				logger.error("Failed to complete a cash-out after settling the final bet.  Manual cash-out will be required for account: {}", accountIdStr, e);
				setStatus(Status.SERVER_ERROR_INTERNAL);
				return prepareErrorResult("Failed to complete cash-out after settling final bet");
			}
		} catch (EOFException | JsonSyntaxException e) {
			setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			return prepareErrorResult("invalid payload format");
		}
	}
}
