/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.roulette;

import com.purplecritter.gamblor.bessy.AbstractApplication;
import com.purplecritter.gamblor.bookie.model.accounts.AccountManager;
import com.purplecritter.gamblor.bookie.model.events.EventManager;
import com.purplecritter.gamblor.bookie.model.seeds.SeedManager;
import com.purplecritter.gamblor.bookie.processor.PaymentProcessor;
import org.restlet.Restlet;
import org.restlet.routing.Router;

public class Roulette extends AbstractApplication {
	public Roulette(PaymentProcessor processor, AccountManager accountManager, EventManager eventManager, SeedManager seedManager) {
		super(processor, accountManager, eventManager, seedManager);
	}

	@Override
	public Restlet createInboundRoot() {
		Router router = new Router(getContext());

		router.attach("/bets", BetResource.class);

		return router;
	}
}
