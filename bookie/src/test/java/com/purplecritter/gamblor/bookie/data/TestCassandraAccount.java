/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.data;

import com.google.gson.JsonObject;
import com.purplecritter.gamblor.bookie.data.accounts.CassandraAccountManager;
import com.purplecritter.gamblor.bookie.model.accounts.Account;
import com.purplecritter.gamblor.bookie.model.bets.BetId;
import com.purplecritter.gamblor.bookie.processor.MockPaymentProcessor;
import com.purplecritter.gamblor.bookie.processor.PaymentProcessor;
import com.purplecritter.gamblor.test.BaseCassandraTest;
import junit.framework.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class TestCassandraAccount extends BaseCassandraTest {
	@Test
	public void testAccountCreation() throws Exception {
		PaymentProcessor processor = new MockPaymentProcessor();
		CassandraAccountManager manager = new CassandraAccountManager(processor, keyspace);

		Account account = manager.create("myReturnAddress");

		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
		Assert.assertTrue(account.getBets().isEmpty());
		Assert.assertTrue(account.getDeposits().isEmpty());

		Assert.assertEquals(account, manager.get(account.getId()));
	}


	@Test
	public void testDeposits() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		CassandraAccountManager manager = new CassandraAccountManager(processor, keyspace);

		Account account = manager.create("myReturnAddress");

		// try a simple deposit
		processor.deposit(account.getDepositAddress(), "myTxnId1", BigDecimal.ONE);
		account = manager.get(account.getId());
		Assert.assertEquals(BigDecimal.ONE, account.getBalance());
		Assert.assertEquals(1, account.getDeposits().size());

		// try to deposit a second transaction
		processor.deposit(account.getDepositAddress(), "myTxnId2", BigDecimal.ONE);
		account = manager.get(account.getId());
		Assert.assertEquals(BigDecimal.valueOf(2), account.getBalance());
		Assert.assertEquals(2, account.getDeposits().size());
	}

	@Test
	public void testBetPlacement() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		CassandraAccountManager manager = new CassandraAccountManager(processor, keyspace);

		Account account = manager.create("myReturnAddress");
		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
		Assert.assertTrue(account.getBets().isEmpty());
		Assert.assertTrue(account.getDeposits().isEmpty());

		// deposit some funds to make a real bet
		processor.deposit(account.getDepositAddress(), "myTxnId1", BigDecimal.ONE);
		account = manager.get(account.getId());
		Assert.assertEquals(BigDecimal.ONE, account.getBalance());
		Assert.assertEquals(1, account.getDeposits().size());

		account.placeBet(BigDecimal.ONE, new JsonObject());
		account = manager.get(account.getId());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
	}

	@Test
	public void testSettlement() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		CassandraAccountManager manager = new CassandraAccountManager(processor, keyspace);

		String returnAddress = "myReturnAddress";

		Account account = manager.create(returnAddress);
		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
		Assert.assertTrue(account.getBets().isEmpty());
		Assert.assertTrue(account.getDeposits().isEmpty());

		// deposit some funds to make a bet
		processor.deposit(account.getDepositAddress(), "myTxnId1", BigDecimal.ONE);
		BetId betId = account.placeBet(BigDecimal.ONE, new JsonObject());

		// settle the bet as a win
		account.settleBet(betId, BigDecimal.TEN);
		account = manager.get(account.getId());
		Assert.assertEquals(1, account.getDeposits().size());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(1, account.getPayouts().size());

		// trigger cashout, with insufficient confirmations
		account.cashOut();
		account = manager.get(account.getId());
		Assert.assertEquals(1, account.getDeposits().size());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(1, account.getPayouts().size());

		// with confirmations, ensure that the account is reset
		processor.setConfirmations("myTxnId1", 1);

		// the account has now cashed out, but not yet archived.  We should be able to see a 0 balance until it gets archived
		account = manager.get(account.getId());
		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
	}
}
