/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.data;

import com.google.gson.JsonObject;
import com.purplecritter.gamblor.bookie.data.accounts.CassandraAccountManager;
import com.purplecritter.gamblor.bookie.data.events.CassandraEventManager;
import com.purplecritter.gamblor.bookie.model.accounts.Account;
import com.purplecritter.gamblor.bookie.model.bets.BetId;
import com.purplecritter.gamblor.bookie.model.events.Event;
import com.purplecritter.gamblor.bookie.processor.MockPaymentProcessor;
import com.purplecritter.gamblor.test.BaseCassandraTest;
import junit.framework.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class TestCassandraEvent extends BaseCassandraTest {
	@Test
	public void testEventCreation() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		CassandraAccountManager accountManager = new CassandraAccountManager(processor, keyspace);
		CassandraEventManager manager = new CassandraEventManager(keyspace, accountManager);

		Event event = manager.create("test");
		event = manager.get(event.getId());
		Assert.assertEquals(0, event.getBets().size());

		event.closeEvent();
		event = manager.get(event.getId());
		Assert.assertEquals(0, event.getBets().size());
		Assert.assertEquals(0, event.getBets().size());
	}

	@Test
	public void testBets() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		CassandraAccountManager accountManager = new CassandraAccountManager(processor, keyspace);
		CassandraEventManager manager = new CassandraEventManager(keyspace, accountManager);

		Account account = accountManager.create("returnAddress");

		Event event = manager.create("test");

		// add some money to the account and place a bet
		processor.deposit(account.getDepositAddress(), "txn1", BigDecimal.ONE);
		BetId betId = event.placeBet(account.getId(), BigDecimal.ONE, new JsonObject());
		event = manager.get(event.getId());
		Assert.assertEquals(1, event.getBets().size());

		event.settleBet(betId, BigDecimal.TEN);
		event = manager.get(event.getId());
		Assert.assertEquals(BigDecimal.TEN, account.getBalance());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(1, account.getPayouts().size());
		Assert.assertEquals(0, event.getBets().size());
	}

	@Test
	public void testLoadingByGame() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		CassandraAccountManager accountManager = new CassandraAccountManager(processor, keyspace);
		CassandraEventManager manager = new CassandraEventManager(keyspace, accountManager);

		manager.create("test1");
		manager.create("test2");

		manager.create("test1");
		manager.create("test2");

		manager.create("test1");

		Assert.assertEquals(3, manager.get("test1").size());
		Assert.assertEquals(2, manager.get("test2").size());
	}
}
