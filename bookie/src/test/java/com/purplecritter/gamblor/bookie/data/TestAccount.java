/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.data;

import com.google.gson.JsonObject;
import com.purplecritter.gamblor.bookie.data.accounts.MockAccountManager;
import com.purplecritter.gamblor.bookie.exceptions.AccountClosedException;
import com.purplecritter.gamblor.bookie.exceptions.InsufficientFundsException;
import com.purplecritter.gamblor.bookie.model.accounts.Account;
import com.purplecritter.gamblor.bookie.model.accounts.AccountManager;
import com.purplecritter.gamblor.bookie.model.bets.BetId;
import com.purplecritter.gamblor.bookie.processor.MockPaymentProcessor;
import com.purplecritter.gamblor.bookie.processor.PaymentProcessor;
import junit.framework.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class TestAccount {
	@Test
	public void testAccountCreation() throws Exception {
		PaymentProcessor processor = new MockPaymentProcessor();
		AccountManager manager = new MockAccountManager(processor);

		Account account = manager.create("myReturnAddress");
		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
		Assert.assertTrue(account.getBets().isEmpty());
		Assert.assertTrue(account.getDeposits().isEmpty());

		Assert.assertEquals(account, manager.get(account.getId()));
	}

	@Test
	public void testDeposits() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		AccountManager manager = new MockAccountManager(processor);

		Account account = manager.create("myReturnAddress");

		// try a simple deposit
		processor.deposit(account.getDepositAddress(), "myTxnId1", BigDecimal.ONE);
		Assert.assertEquals(BigDecimal.ONE, account.getBalance());
		Assert.assertEquals(1, account.getDeposits().size());

		// try to deposit the exact same transaction a second time
		processor.deposit(account.getDepositAddress(), "myTxnId1", BigDecimal.ONE);
		Assert.assertEquals(BigDecimal.ONE, account.getBalance());
		Assert.assertEquals(1, account.getDeposits().size());

		// try to deposit a second transaction
		processor.deposit(account.getDepositAddress(), "myTxnId2", BigDecimal.ONE);
		Assert.assertEquals(BigDecimal.valueOf(2), account.getBalance());
		Assert.assertEquals(2, account.getDeposits().size());
	}

	@Test
	public void testBetPlacement() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		AccountManager manager = new MockAccountManager(processor);

		Account account = manager.create("myReturnAddress");
		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
		Assert.assertTrue(account.getBets().isEmpty());
		Assert.assertTrue(account.getDeposits().isEmpty());

		try {
			account.placeBet(BigDecimal.ONE, new JsonObject());
			Assert.fail("insufficient funds to place this bet");
		} catch (InsufficientFundsException e) {
			// expected
		}

		// deposit some funds to make a real bet
		processor.deposit(account.getDepositAddress(), "myTxnId1", BigDecimal.ONE);
		Assert.assertEquals(BigDecimal.ONE, account.getBalance());
		Assert.assertEquals(1, account.getDeposits().size());

		account.placeBet(BigDecimal.ONE, new JsonObject());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());

		// deposit some more funds and try some partial bets
		processor.deposit(account.getDepositAddress(), "myTxnId2", BigDecimal.ONE);
		Assert.assertEquals(BigDecimal.ONE, account.getBalance());
		Assert.assertEquals(2, account.getDeposits().size());

		account.placeBet(BigDecimal.valueOf(0.5), new JsonObject());
		Assert.assertEquals(2, account.getBets().size());
		Assert.assertEquals(BigDecimal.valueOf(0.5), account.getBalance());

		account.placeBet(BigDecimal.valueOf(0.5), new JsonObject());
		Assert.assertEquals(3, account.getBets().size());
		Assert.assertEquals(BigDecimal.valueOf(0.0), account.getBalance());

		// deposit some more funds and try some invalid bets
		processor.deposit(account.getDepositAddress(), "myTxnId3", BigDecimal.ONE);
		Assert.assertEquals(BigDecimal.valueOf(1.0), account.getBalance());
		Assert.assertEquals(3, account.getDeposits().size());

		try {
			account.placeBet(BigDecimal.valueOf(0.0), new JsonObject());
			Assert.fail("stake must be > 0");
		} catch (IllegalArgumentException e) {
			// expected
			Assert.assertEquals(3, account.getBets().size());
			Assert.assertEquals(BigDecimal.valueOf(1.0), account.getBalance());
		}

		try {
			account.placeBet(BigDecimal.ZERO, new JsonObject());
			Assert.fail("stake must be > 0");
		} catch (IllegalArgumentException e) {
			// expected
			Assert.assertEquals(3, account.getBets().size());
			Assert.assertEquals(BigDecimal.valueOf(1.0), account.getBalance());
		}

		try {
			account.placeBet(BigDecimal.valueOf(-1.0), new JsonObject());
			Assert.fail("stake must be > 0");
		} catch (IllegalArgumentException e) {
			// expected
			Assert.assertEquals(3, account.getBets().size());
			Assert.assertEquals(BigDecimal.valueOf(1.0), account.getBalance());
		}
	}

	private static class CashOutListener implements MockPaymentProcessor.PaymentListener {
		private BigDecimal lastAmount = null;

		@Override
		public void onPayment(BigDecimal amount) {
			lastAmount = amount;
		}

		public BigDecimal getLastAmount() {
			return lastAmount;
		}
	}

	@Test
	public void testCashOut() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		AccountManager manager = new MockAccountManager(processor);
		CashOutListener listener = new CashOutListener();

		String returnAddress = "myReturnAddress";
		processor.addListener(returnAddress, listener);

		Account account = manager.create(returnAddress);
		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
		Assert.assertEquals(0, account.getPayouts().size());
		Assert.assertEquals(0, account.getDeposits().size());
		Assert.assertEquals(0, account.getBets().size());
		Assert.assertEquals(null, listener.getLastAmount());

		account.cashOut();
		Assert.assertEquals(null, listener.getLastAmount());

		/*
		 * try making sure that deposits work after the correct number of confirmations
		 */
		account = manager.create(returnAddress);
		processor.deposit(account.getDepositAddress(), "myTxnId1", BigDecimal.ONE);
		processor.setConfirmations("myTxnId1", 1);
		account.cashOut();
		Assert.assertEquals(BigDecimal.ONE, listener.getLastAmount());

		account = manager.create(returnAddress);
		processor.deposit(account.getDepositAddress(), "myTxnId2", BigDecimal.valueOf(5.1));
		processor.setConfirmations("myTxnId2", 2);
		account.cashOut();
		Assert.assertEquals(BigDecimal.valueOf(5.1), listener.getLastAmount());

		account = manager.create(returnAddress);
		processor.deposit(account.getDepositAddress(), "myTxnId3", BigDecimal.valueOf(10.1));
		processor.setConfirmations("myTxnId3", 3);
		account.cashOut();
		Assert.assertEquals(BigDecimal.valueOf(10.1), listener.getLastAmount());

		account = manager.create(returnAddress);
		processor.deposit(account.getDepositAddress(), "myTxnId4", BigDecimal.valueOf(15.1));
		processor.setConfirmations("myTxnId4", 4);
		account.cashOut();
		Assert.assertEquals(BigDecimal.valueOf(15.1), listener.getLastAmount());

		account = manager.create(returnAddress);
		processor.deposit(account.getDepositAddress(), "myTxnId4", BigDecimal.valueOf(20.1));
		processor.setConfirmations("myTxnId4", 6);
		account.cashOut();
		Assert.assertEquals(BigDecimal.valueOf(20.1), listener.getLastAmount());

		/*
		 * try cashing out with insufficient confirmations and then see if the cashout happens when
		 * the confirmations become sufficient
		 */

		account = manager.create(returnAddress);
		processor.deposit(account.getDepositAddress(), "myTxnId5", BigDecimal.valueOf(1.1));
		processor.setConfirmations("myTxnId5", 0);
		account.cashOut();
		Assert.assertEquals(BigDecimal.valueOf(20.1), listener.getLastAmount());
		processor.setConfirmations("myTxnId5", 1);
		Assert.assertEquals(BigDecimal.valueOf(1.1), listener.getLastAmount());

		account = manager.create(returnAddress);
		processor.deposit(account.getDepositAddress(), "myTxnId6", BigDecimal.valueOf(5.1));
		processor.setConfirmations("myTxnId6", 0);
		account.cashOut();
		Assert.assertEquals(BigDecimal.valueOf(1.1), listener.getLastAmount());
		processor.setConfirmations("myTxnId6", 1);
		Assert.assertEquals(BigDecimal.valueOf(1.1), listener.getLastAmount());
		processor.setConfirmations("myTxnId6", 2);
		Assert.assertEquals(BigDecimal.valueOf(5.1), listener.getLastAmount());

		account = manager.create(returnAddress);
		processor.deposit(account.getDepositAddress(), "myTxnId7", BigDecimal.valueOf(10.1));
		processor.setConfirmations("myTxnId7", 0);
		account.cashOut();
		Assert.assertEquals(BigDecimal.valueOf(5.1), listener.getLastAmount());
		processor.setConfirmations("myTxnId7", 1);
		Assert.assertEquals(BigDecimal.valueOf(5.1), listener.getLastAmount());
		processor.setConfirmations("myTxnId7", 2);
		Assert.assertEquals(BigDecimal.valueOf(5.1), listener.getLastAmount());
		processor.setConfirmations("myTxnId7", 3);
		Assert.assertEquals(BigDecimal.valueOf(10.1), listener.getLastAmount());
	}

	@Test
	public void testSettlement() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		AccountManager manager = new MockAccountManager(processor);
		CashOutListener listener = new CashOutListener();

		String returnAddress = "myReturnAddress";
		processor.addListener(returnAddress, listener);

		Account account = manager.create(returnAddress);
		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
		Assert.assertTrue(account.getBets().isEmpty());
		Assert.assertTrue(account.getDeposits().isEmpty());

		// deposit some funds to make a bet
		processor.deposit(account.getDepositAddress(), "myTxnId1", BigDecimal.ONE);
		BetId betId = account.placeBet(BigDecimal.ONE, new JsonObject());

		// settle the bet as a win
		account.settleBet(betId, BigDecimal.TEN);
		account.cashOut();

		// at this point, there are insufficient confirmations to cashout so no funds should be received
		Assert.assertEquals(null, listener.getLastAmount());

		// the cashout is pending, so shouldn't be able to place bets now
		try {
			account.placeBet(BigDecimal.ONE, new JsonObject());
			Assert.fail("shouldn't be able to place a bet after a cash-out has been requested");
		} catch (AccountClosedException e) {
			// expected
		}

		processor.setConfirmations("myTxnId1", 1);
		Assert.assertEquals(BigDecimal.valueOf(10), listener.getLastAmount());
		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(1, account.getDeposits().size());
		Assert.assertEquals(1, account.getPayouts().size());

		account.settleBet(betId, BigDecimal.ZERO);
		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(1, account.getDeposits().size());
		Assert.assertEquals(1, account.getPayouts().size());
	}

	@Test
	public void testSettlement2() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		AccountManager manager = new MockAccountManager(processor);
		CashOutListener listener = new CashOutListener();

		String returnAddress = "myReturnAddress";
		processor.addListener(returnAddress, listener);

		Account account = manager.create(returnAddress);

		// try with multiple cashouts to ensure account balance stays in sync
		processor.deposit(account.getDepositAddress(), "myTxnId2", BigDecimal.ONE);
		BetId betId = account.placeBet(BigDecimal.ONE, new JsonObject());
		processor.deposit(account.getDepositAddress(), "myTxnId3", BigDecimal.ONE);

		account.cashOut();

		// at this point, there are no confirmations, so the cashout wont be processed yet
		Assert.assertEquals(null, listener.getLastAmount());
		Assert.assertEquals(BigDecimal.ONE, account.getBalance());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(2, account.getDeposits().size());
		Assert.assertEquals(0, account.getPayouts().size());

		// the first deposit is confirmed, but the second one isn't
		processor.setConfirmations("myTxnId2", 1);
		Assert.assertEquals(null, listener.getLastAmount());
		Assert.assertEquals(BigDecimal.ONE, account.getBalance());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(2, account.getDeposits().size());
		Assert.assertEquals(0, account.getPayouts().size());

		// the second deposit is confirmed, but the bet hast been settled yet
		processor.setConfirmations("myTxnId3", 1);
		Assert.assertEquals(null, listener.getLastAmount());
		Assert.assertEquals(BigDecimal.ONE, account.getBalance());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(2, account.getDeposits().size());
		Assert.assertEquals(0, account.getPayouts().size());

		// the bet has been settled, so the cashout should proceed
		account.settleBet(betId, BigDecimal.valueOf(2));
		Assert.assertEquals(BigDecimal.valueOf(3), listener.getLastAmount());
		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(2, account.getDeposits().size());
		Assert.assertEquals(1, account.getPayouts().size());
	}

	@Test
	public void testSettlement3() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		AccountManager manager = new MockAccountManager(processor);
		CashOutListener listener = new CashOutListener();

		String returnAddress = "myReturnAddress";
		processor.addListener(returnAddress, listener);

		Account account = manager.create(returnAddress);

		// try placing a bet with a double-spend (ie: no money returned)
		processor.deposit(account.getDepositAddress(), "myTxnId1", BigDecimal.ONE);
		processor.setConfirmations("myTxnId1", -1);

		try {
			account.placeBet(BigDecimal.ONE, new JsonObject());
			Assert.fail("shouldn't be able to place a bet on a double-spend");
		} catch (AccountClosedException e) {
			// expected
		}

		Assert.assertEquals(null, listener.getLastAmount());
		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
		Assert.assertEquals(0, account.getBets().size());
		Assert.assertEquals(1, account.getDeposits().size());
		Assert.assertEquals(0, account.getPayouts().size());

		account.cashOut();
		Assert.assertEquals(null, listener.getLastAmount());
		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
		Assert.assertEquals(0, account.getBets().size());
		Assert.assertEquals(1, account.getDeposits().size());
		Assert.assertEquals(0, account.getPayouts().size());
	}

	@Test
	public void testSettlement4() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		AccountManager manager = new MockAccountManager(processor);
		CashOutListener listener = new CashOutListener();

		String returnAddress = "myReturnAddress";
		processor.addListener(returnAddress, listener);

		Account account = manager.create(returnAddress);

		// cheat scenario: deposit, place bets and lose, deposit double-spend, cashout
		processor.deposit(account.getDepositAddress(), "myTxnId1", BigDecimal.ONE);
		BetId betId = account.placeBet(BigDecimal.ONE, new JsonObject());
		account.settleBet(betId, BigDecimal.TEN);

		Assert.assertEquals(null, listener.getLastAmount());
		Assert.assertEquals(BigDecimal.TEN, account.getBalance());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(1, account.getDeposits().size());
		Assert.assertEquals(1, account.getPayouts().size());

		// try to cash out and make sure nothing gets transferred
		account.cashOut();
		Assert.assertEquals(null, listener.getLastAmount());
		Assert.assertEquals(BigDecimal.TEN, account.getBalance());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(1, account.getDeposits().size());
		Assert.assertEquals(1, account.getPayouts().size());

		processor.setConfirmations("myTxnId1", -1);
		Assert.assertEquals(null, listener.getLastAmount());
		Assert.assertEquals(BigDecimal.ZERO, account.getBalance());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(1, account.getDeposits().size());
		Assert.assertEquals(1, account.getPayouts().size());

		try {
			account.placeBet(BigDecimal.ONE, new JsonObject());
			Assert.fail("shouldn't be able to place a bet on a double-spend");
		} catch (AccountClosedException e) {
			// expected
		}
	}
}