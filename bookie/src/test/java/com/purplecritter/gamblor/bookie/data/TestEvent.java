/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.data;

import com.google.gson.JsonObject;
import com.purplecritter.gamblor.bookie.data.accounts.MockAccountManager;
import com.purplecritter.gamblor.bookie.exceptions.EventClosedException;
import com.purplecritter.gamblor.bookie.exceptions.InsufficientFundsException;
import com.purplecritter.gamblor.bookie.model.accounts.Account;
import com.purplecritter.gamblor.bookie.model.bets.BetId;
import com.purplecritter.gamblor.bookie.model.events.Event;
import com.purplecritter.gamblor.bookie.processor.MockPaymentProcessor;
import junit.framework.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class TestEvent {

	@Test
	public void testEventCreation() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		MockAccountManager accountManager = new MockAccountManager(processor);
		MockEventManager manager = new MockEventManager(accountManager);

		Event event = manager.create("test");
		Assert.assertEquals(0, event.getBets().size());

		event.closeEvent();
		Assert.assertEquals(0, event.getBets().size());
	}

	@Test
	public void testBets() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		MockAccountManager accountManager = new MockAccountManager(processor);
		MockEventManager manager = new MockEventManager(accountManager);

		Account account = accountManager.create("returnAddress");

		Event event = manager.create("test");

		// place a bet with no money
		try {
			event.placeBet(account.getId(), BigDecimal.ONE, new JsonObject());
			Assert.fail("shouldn't be able ot place bets without any money in the account");
		} catch (InsufficientFundsException e) {
			// expected
		}
		Assert.assertEquals(0, event.getBets().size());

		// add some money to the account and try again
		processor.deposit(account.getDepositAddress(), "txn1", BigDecimal.ONE);
		BetId betId = event.placeBet(account.getId(), BigDecimal.ONE, new JsonObject());
		Assert.assertEquals(1, event.getBets().size());

		event.settleBet(betId, BigDecimal.TEN);
		Assert.assertEquals(BigDecimal.TEN, account.getBalance());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(1, account.getPayouts().size());
		Assert.assertEquals(0, event.getBets().size());
	}

	@Test
	public void testPlacingBetAfterClosingEvent() throws Exception {
		MockPaymentProcessor processor = new MockPaymentProcessor();
		MockAccountManager accountManager = new MockAccountManager(processor);
		MockEventManager manager = new MockEventManager(accountManager);

		Account account = accountManager.create("returnAddress");

		Event event = manager.create("test");

		processor.deposit(account.getDepositAddress(), "txn1", BigDecimal.ONE);
		BetId betId = event.placeBet(account.getId(), BigDecimal.ONE, new JsonObject());
		Assert.assertEquals(1, event.getBets().size());

		event.closeEvent();

		try {
			event.placeBet(account.getId(), BigDecimal.ONE, new JsonObject());
			Assert.fail("shouldn't be able to place a bet after the event has been closed");
		} catch (EventClosedException e) {
			// expected
		}
		Assert.assertEquals(1, event.getBets().size());

		event.settleBet(betId, BigDecimal.TEN);
		Assert.assertEquals(BigDecimal.TEN, account.getBalance());
		Assert.assertEquals(1, account.getBets().size());
		Assert.assertEquals(1, account.getPayouts().size());
	}
}
