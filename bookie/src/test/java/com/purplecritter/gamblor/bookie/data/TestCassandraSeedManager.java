/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.data;

import com.google.common.hash.HashCodes;
import com.purplecritter.gamblor.bookie.model.seeds.Seed;
import com.purplecritter.gamblor.bookie.model.seeds.SeedManager;
import com.purplecritter.gamblor.test.BaseCassandraTest;
import junit.framework.Assert;
import org.junit.Test;

public class TestCassandraSeedManager extends BaseCassandraTest {
	@Test
	public void testSeeds() throws Exception {
		SeedManager manager = new CassandraSeedManager(keyspace);

		// nothing should be persisted, so we should never find anything here
		Assert.assertNull(manager.get(HashCodes.fromLong(284724363527432l)));

		// try creating one and verify its valid
		Seed seed1 = manager.create();
		Assert.assertNotNull(seed1);
		Assert.assertNotNull(seed1.getHash());
		Assert.assertNotNull(seed1.getSeed());

		// create a second one and make sure its not the same as the first
		Seed seed2 = manager.create();
		Assert.assertNotNull(seed2);
		Assert.assertNotNull(seed2.getHash());
		Assert.assertNotNull(seed2.getSeed());
		Assert.assertFalse(seed1.getHash().equals(seed2.getHash()));

		// fetch the first one and make sure we got the right one back
		Seed fetched = manager.get(seed1.getHash());
		Assert.assertNotNull(fetched);
		Assert.assertEquals(seed1, fetched);

		// fetch the second one and make sure we got the right one back
		fetched = manager.get(seed2.getHash());
		Assert.assertNotNull(fetched);
		Assert.assertEquals(seed2, fetched);
	}
}
