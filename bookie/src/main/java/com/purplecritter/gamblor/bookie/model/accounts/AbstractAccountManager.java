/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.model.accounts;

import com.purplecritter.gamblor.bookie.Util;
import com.purplecritter.gamblor.bookie.exceptions.PaymentException;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.model.TransactionDetails;
import com.purplecritter.gamblor.bookie.model.deposits.Deposit;
import com.purplecritter.gamblor.bookie.processor.PaymentProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

public abstract class AbstractAccountManager implements AccountManager {
	private static final Logger logger = LoggerFactory.getLogger(AbstractAccountManager.class);

	protected final PaymentProcessor processor;

	public AbstractAccountManager(PaymentProcessor processor) {
		if (processor == null) {
			throw new IllegalArgumentException("processor is required");
		}

		this.processor = processor;
		this.processor.registerBlockListener(new PaymentProcessor.BlockListener() {
			@Override
			public void onBlockFound() {
				for (Account account : getAccounts()) {
					try {
						for (Deposit deposit : account.getDeposits()) {
							if (!deposit.isConfirmed() && !deposit.isDoubleSpend()) {
								TransactionDetails details = AbstractAccountManager.this.processor.getTransactionDetails(deposit.getTxid());
								if (details.getConfirmations() < 0) {
									mutate(account).setDoubleSpend(deposit.getTxid()).apply();
								} else if (details.getConfirmations() >= Util.calculateMinConfirmations(deposit.getAmount())) {
									mutate(account).setConfirmed(deposit.getTxid()).apply();
								}
							}
						}

						if (account.isCashingOut()) {
							account.attemptToProcessCashOut();
						}
					} catch (StoreException | PaymentException e) {
						logger.warn("Unable to process cash-out for account {}", account.getId(), e);
					}
				}
			}
		});
	}

	protected abstract Collection<? extends Account> getAccounts();
}
