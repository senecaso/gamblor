/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.model.payouts;

import com.purplecritter.gamblor.bookie.model.Unique;
import com.purplecritter.gamblor.bookie.model.bets.BetId;

import java.math.BigDecimal;

public class Payout implements Unique {
	private final PayoutId id;
	private final BetId betId;
	private final BigDecimal payout;

	public Payout(PayoutId id, BetId betId, BigDecimal payout) {
		if (id == null) {
			throw new IllegalArgumentException("id is required");
		}

		if (betId == null) {
			throw new IllegalArgumentException("betId is required");
		}

		if (payout == null || payout.compareTo(BigDecimal.ZERO) < 0) {
			throw new IllegalArgumentException("payout must be >= 0");
		}

		this.id = id;
		this.betId = betId;
		this.payout = payout;
	}

	public PayoutId getId() {
		return id;
	}

	public BetId getBetId() {
		return betId;
	}

	public BigDecimal getPayout() {
		return payout;
	}

	@Override
	public String toString() {
		return id + ":" + payout;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Payout payout = (Payout) o;

		if (!id.equals(payout.id)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}
}
