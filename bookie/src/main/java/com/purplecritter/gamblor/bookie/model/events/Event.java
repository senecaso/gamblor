/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.model.events;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import com.purplecritter.gamblor.bookie.exceptions.AccountClosedException;
import com.purplecritter.gamblor.bookie.exceptions.EventClosedException;
import com.purplecritter.gamblor.bookie.exceptions.InsufficientFundsException;
import com.purplecritter.gamblor.bookie.exceptions.LoadException;
import com.purplecritter.gamblor.bookie.exceptions.PaymentException;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.exceptions.UnknownAccountException;
import com.purplecritter.gamblor.bookie.model.Unique;
import com.purplecritter.gamblor.bookie.model.accounts.Account;
import com.purplecritter.gamblor.bookie.model.accounts.AccountId;
import com.purplecritter.gamblor.bookie.model.accounts.AccountManager;
import com.purplecritter.gamblor.bookie.model.bets.BetId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Event implements Unique {
	private static final Logger logger = LoggerFactory.getLogger(Event.class);

	protected final Object lock = new Object();

	private final AccountManager accountManager;
	private final EventManager eventManager;

	private final EventId eventId;

	private final String game;
	private Map<BetId, AccountId> bets = new HashMap<>();
	private Set<BetId> settledBets = new HashSet<>();
	private boolean isClosed = false;

	public Event(AccountManager accountManager, EventManager eventManager, EventId eventId, String game) {
		if (accountManager == null) {
			throw new IllegalArgumentException("accountManager is required");
		}

		if (eventManager == null) {
			throw new IllegalArgumentException("eventManager is required");
		}

		if (eventId == null) {
			throw new IllegalArgumentException("eventId is required");
		}

		if (Strings.isNullOrEmpty(game)) {
			throw new IllegalArgumentException("game is required");
		}

		this.accountManager = accountManager;
		this.eventManager = eventManager;
		this.eventId = eventId;
		this.game = game;
	}

	public EventId getId() {
		return eventId;
	}

	public String getGame() {
		return game;
	}

	public BetId placeBet(AccountId accountId, BigDecimal stake, JsonObject outcome) throws EventClosedException, InsufficientFundsException, UnknownAccountException, LoadException, StoreException, AccountClosedException {
		if (accountId == null) {
			throw new IllegalArgumentException("invalid accountId");
		}

		if (stake == null || stake.compareTo(BigDecimal.ZERO) <= 0) {
			throw new IllegalArgumentException("invalid stake");
		}

		if (outcome == null) {
			throw new IllegalArgumentException("outcome is required");
		}

		BetId betId;
		synchronized (lock) {
			if (isClosed) {
				throw new EventClosedException(eventId);
			}

			logger.info("placing bet on event {} for a stake of {}", new Object[] {eventId, stake});

			Account account = accountManager.get(accountId);
			if (account == null) {
				throw new UnknownAccountException();
			}

			betId = account.placeBet(stake, outcome);

			eventManager.mutate(this).addBet(betId, accountId).apply();
		}

		logger.info("placed bet {} on event {} for a stake of {}", new Object[]{betId, eventId, stake});
		return betId;
	}

	public void closeEvent() throws StoreException {
		logger.info("closing event {}", eventId);

		synchronized (lock) {
			if (!isClosed) {
				eventManager.mutate(this).closeEvent().apply();
			}
		}
	}

	public void settleBet(BetId betId, BigDecimal payout) throws LoadException, StoreException, PaymentException {
		if (betId == null) {
			throw new IllegalArgumentException("betId is required");
		}

		if (payout == null || payout.compareTo(BigDecimal.ZERO) < 0) {
			throw new IllegalArgumentException("payout must be >= 0");
		}

		logger.info("settling bet {} from event {} with a payout of {}", new Object[]{betId, eventId, payout});

		if (payout.compareTo(BigDecimal.ZERO) > 0) {
			synchronized (lock) {
				AccountId accountId = bets.get(betId);
				if (accountId != null) {
					Account account = accountManager.get(accountId);
					account.settleBet(betId, payout);

					eventManager.mutate(this).settleBet(betId).apply();
				} else {
					logger.info("Unknown bet {} in account {}.  This bet was either previously settled, or this bet never existed", betId, accountId);
				}
			}
		}

		logger.info("settled bet {} from event {} with a payout of {}", new Object[]{betId, eventId, payout});
	}

	@Override
	public String toString() {
		return "event-" + eventId + ":" + bets.size();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Event event = (Event) o;

		if (!eventId.equals(event.eventId)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return eventId.hashCode();
	}

	public void setIsClosed(boolean isClosed) {
		synchronized (lock) {
			this.isClosed = isClosed;
		}
	}

	public boolean isClosed() {
		synchronized (lock) {
			return isClosed;
		}
	}

	public void setBets(Map<BetId, AccountId> bets) {
		this.bets = bets;
	}

	public Map<BetId, AccountId> getBets() {
		return Collections.unmodifiableMap(bets);
	}

	public void setSettledBets(Set<BetId> settledBets) {
		this.settledBets = settledBets;
	}

	public Set<BetId> getSettledBets() {
		return Collections.unmodifiableSet(settledBets);
	}

	public Set<BetId> getUnsettledBets() {
		Set<BetId> unsettledBets = new HashSet<>();
		unsettledBets.addAll(bets.keySet());
		unsettledBets.removeAll(settledBets);

		return unsettledBets;
	}
}
