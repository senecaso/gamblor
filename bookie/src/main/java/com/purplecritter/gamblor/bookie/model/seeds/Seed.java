/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.model.seeds;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;

import java.security.SecureRandom;
import java.util.Arrays;

public class Seed {
	public static final int SEED_LEN = 32;
	private static final SecureRandom rand = new SecureRandom();

	private final HashCode hash;
	private final byte[] seed;

	public Seed(HashCode hash, byte[] seed) {
		this.hash = hash;
		this.seed = seed;

		if (!Hashing.sha256().hashBytes(seed).equals(hash)) {
			throw new IllegalArgumentException("Invalid hashcode for the given seed");
		}
	}

	public Seed() {
		seed = new byte[SEED_LEN];
		rand.nextBytes(seed);

		hash = Hashing.sha256().hashBytes(seed);
	}

	public HashCode getHash() {
		return hash;
	}

	public byte[] getSeed() {
		return seed;
	}

	@Override
	public String toString() {
		return hash.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Seed seed1 = (Seed) o;

		if (!hash.equals(seed1.hash)) return false;
		if (!Arrays.equals(seed, seed1.seed)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = hash.hashCode();
		result = 31 * result + Arrays.hashCode(seed);
		return result;
	}
}