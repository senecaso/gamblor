/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.data;

import com.google.common.collect.ImmutableMap;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.MutationBatch;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.connectionpool.exceptions.NotFoundException;
import com.netflix.astyanax.model.Column;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.serializers.StringSerializer;

public class MiscStorage {
	private static final String CF = "miscStorage";
	private static final String VALUE = "value";

	private final Keyspace ks;
	private final ColumnFamily<String, String> cf;

	public MiscStorage(Keyspace ks) throws ConnectionException {
		this.ks = ks;
		this.cf = new ColumnFamily<>(CF, StringSerializer.get(), StringSerializer.get());

		if (ks.describeKeyspace().getColumnFamily(CF) == null) {
			ks.createColumnFamily(cf, ImmutableMap.<String, Object>builder()
					.put("default_validation_class", "BytesType")
					.put("key_validation_class", "UTF8Type")
					.put("comparator_type", "UTF8Type")
					.build());
		}
	}

	public Integer get(String key) throws ConnectionException {
		try {
			Column<String> column = ks.prepareQuery(cf).getRow(key).getColumn(VALUE).execute().getResult();
			return column.hasValue() ? column.getIntegerValue() : null;
		} catch (NotFoundException e) {
			return null;
		}
	}

	public void put(String key, int value) throws ConnectionException {
		MutationBatch mb = ks.prepareMutationBatch();
		mb.withRow(cf, key).putColumn(VALUE, value);
		mb.execute();
	}
}
