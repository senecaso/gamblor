/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.processor;

import com.purplecritter.gamblor.bookie.exceptions.PaymentException;
import com.purplecritter.gamblor.bookie.model.TransactionDetails;
import com.purplecritter.gamblor.bookie.model.accounts.AccountId;
import com.purplecritter.gamblor.bookie.model.cashouts.Txn;

import java.math.BigDecimal;

public interface PaymentProcessor {
	public static abstract class DepositListener {
		protected final AccountId accountId;

		public DepositListener(AccountId accountId) {
			this.accountId = accountId;
		}

		public abstract  void onDepositReceived(String txid);
	}

	public static interface BlockListener {
		public void onBlockFound();
	}

	public Txn prepareTransaction(String destAddress, BigDecimal amount) throws PaymentException;

	public void submitTransaction(Txn txn) throws PaymentException;

	public String createAddress();

	public void setDepositListener(String depositAddress, DepositListener listener);
	public void removeDepositListener(String depositAddress);

	public boolean isValidAddress(String address);

	public TransactionDetails getTransactionDetails(String txid);

	/**
	 * reprocess any transactions which occurred since the last time a block was successfully processed.  This
	 * will ensure that any transactions which occurred after a crash are replayed so that they aren't missed
	 */
	public void processMissedTransactions();

	public void registerBlockListener(BlockListener listener);
	public void unregisterBlockListener(BlockListener listener);
}
