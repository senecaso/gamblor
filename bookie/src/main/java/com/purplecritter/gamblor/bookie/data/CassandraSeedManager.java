/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.data;

import com.google.common.collect.ImmutableMap;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashCodes;
import com.netflix.astyanax.ExceptionCallback;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.MutationBatch;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.model.ColumnList;
import com.netflix.astyanax.model.Row;
import com.netflix.astyanax.model.Rows;
import com.netflix.astyanax.serializers.BytesArraySerializer;
import com.netflix.astyanax.serializers.StringSerializer;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.model.seeds.Seed;
import com.purplecritter.gamblor.bookie.model.seeds.SeedManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CassandraSeedManager implements SeedManager {
	private static final Logger logger = LoggerFactory.getLogger(CassandraSeedManager.class);

	private static final String SEED = "seed";
	private static final String CF = "seeds";

	private final Keyspace ks;
	private final ColumnFamily<byte[], String> cf;

	private final Map<HashCode, Seed> seeds = new ConcurrentHashMap<>();

	public CassandraSeedManager(Keyspace ks) throws ConnectionException {
		if (ks == null) {
			throw new IllegalArgumentException("ks is required");
		}

		this.ks = ks;

		cf = new ColumnFamily<>(CF, BytesArraySerializer.get(), StringSerializer.get());

		if (ks.describeKeyspace().getColumnFamily(CF) == null) {
			ks.createColumnFamily(cf, ImmutableMap.<String, Object>builder()
					.put("default_validation_class", "BytesType")
					.put("key_validation_class", "BytesType")
					.put("comparator_type", "UTF8Type")
					.build());
		}

		Rows<byte[], String> rows;
		try {
			rows = ks.prepareQuery(cf)
					.getAllRows()
					.setRowLimit(50)
					.setExceptionCallback(new ExceptionCallback() {
						@Override
						public boolean onException(ConnectionException e) {
							logger.error("Unable to connect to cassandra", e);
							return false; // dont bother retrying
						}
					})
					.execute().getResult();
		} catch (ConnectionException e) {
			logger.error("Unable to connect to cassandra", e);
			throw new RuntimeException(e);
		}

		int count = 0;
		for (Row<byte[], String> row : rows) {
			ColumnList<String> columns = row.getColumns();
			HashCode hash = HashCodes.fromBytes(row.getKey());
			byte[] seedBytes = columns.getColumnByIndex(0).getByteArrayValue();
			if (seedBytes != null) {
				count++;
				seeds.put(hash, new Seed(hash, seedBytes));
			}
		}

		logger.info("Loaded {} seeds from storage", count);
	}

	@Override
	public Seed create() throws StoreException {
		try {
			Seed seed = new Seed();
			MutationBatch m = ks.prepareMutationBatch();
			m.withRow(cf, seed.getHash().asBytes()).putColumn(SEED, seed.getSeed());
			m.execute();

			seeds.put(seed.getHash(), seed);
			logger.debug("Created seed {}", seed.getHash());

			return seed;
		} catch (ConnectionException e) {
			throw new StoreException(e);
		}
	}

	@Override
	public Seed get(HashCode hash) {
		Seed seed = seeds.get(hash);
		if (seed == null) {
			logger.debug("Invalid seed {}", hash);
		}

		return seed;
	}

	@Override
	public void remove(HashCode hash) {
		Seed seed = seeds.remove(hash);
		if (seed != null) {
			try {
				MutationBatch m = ks.prepareMutationBatch();
				m.withRow(cf, hash.asBytes()).delete();
				m.execute();
			} catch (ConnectionException e) {
				logger.error("Failed to remove seed {}", hash, e);
			}
		}
	}
}
