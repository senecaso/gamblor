/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.model.events;

import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.model.accounts.AccountId;
import com.purplecritter.gamblor.bookie.model.bets.BetId;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public abstract class AbstractEventMutation implements EventManager.Mutation {
	protected final Event event;
	protected boolean isClosed;
	protected Map<BetId, AccountId> bets;
	protected Set<BetId> settledBets;

	public AbstractEventMutation(Event event) {
		this.event = event;
		bets = new HashMap<>(event.getBets());
		isClosed = event.isClosed();
	}

	@Override
	public EventManager.Mutation addBet(BetId betId, AccountId accountId) {
		bets.put(betId, accountId);
		return this;
	}

	@Override
	public EventManager.Mutation settleBet(BetId betId) {
		settledBets.add(betId);
		return this;
	}

	@Override
	public EventManager.Mutation closeEvent() {
		isClosed = true;
		return this;
	}

	@Override
	public void apply() throws StoreException {
		event.setIsClosed(isClosed);
		event.setBets(bets);
		event.setSettledBets(settledBets);
	}
}
