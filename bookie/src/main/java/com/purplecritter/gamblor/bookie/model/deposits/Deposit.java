/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.model.deposits;

import java.math.BigDecimal;

public class Deposit {
	private final String txid;
	private final BigDecimal amount;
	private final boolean isConfirmed;
	private final boolean isDoubleSpend;

	public Deposit(String txid, BigDecimal amount, boolean confirmed, boolean isDoubleSpend) {
		if (txid == null || txid.trim().isEmpty()) {
			throw new IllegalArgumentException("txid is required");
		}

		if (amount == null || amount.compareTo(BigDecimal.ZERO) <= 0) {
			throw new IllegalArgumentException("amount must be > 0");
		}

		if (confirmed && isDoubleSpend) {
			throw new IllegalArgumentException("a deposit cannot be confirmed AND a double-spend");
		}

		this.txid = txid;
		this.amount = amount;
		isConfirmed = confirmed;
		this.isDoubleSpend = isDoubleSpend;
	}

	public String getTxid() {
		return txid;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public boolean isConfirmed() {
		return isConfirmed;
	}

	public boolean isDoubleSpend() {
		return isDoubleSpend;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Deposit deposit = (Deposit) o;

		if (!txid.equals(deposit.txid)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return txid.hashCode();
	}
}
