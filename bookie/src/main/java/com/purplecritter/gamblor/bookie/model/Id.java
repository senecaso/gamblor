/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.model;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Arrays;

public abstract class Id implements Comparable<Id> {
	private static final int ID_LEN = 16;

	private static final SecureRandom rand = new SecureRandom();

	private final byte[] id;

	protected static byte[] randomId() {
		byte[] id = new byte[ID_LEN];
		rand.nextBytes(id);

		long t = System.currentTimeMillis();
		t = t << 16;

		id[0] = (byte)(0xff & (t >> 56));
		id[1] = (byte)(0xff & (t >> 48));
		id[2] = (byte)(0xff & (t >> 40));
		id[3] = (byte)(0xff & (t >> 32));
		id[4] = (byte)(0xff & (t >> 24));
		id[5] = (byte)(0xff & (t >> 16));

		return id;
	}

	protected Id(byte[] id) {
		this.id = id;
	}

	public Id(String id) {
		if (id == null || id.trim().isEmpty()) {
			throw new IllegalArgumentException("id must be defined");
		}

		try {
			this.id = Hex.decodeHex(id.toCharArray());
			if (this.id.length != ID_LEN) {
				throw new IllegalArgumentException("id must be " + ID_LEN + " bytes");
			}
		} catch (DecoderException e) {
			throw new IllegalArgumentException("Invalid id given: " + id, e);
		}
	}

	public long getTimestamp() {
		long t = (((long)(id[0] & 0xff) << 56) |
				 ((long)(id[1] & 0xff) << 48) |
				 ((long)(id[2] & 0xff) << 40) |
				 ((long)(id[3] & 0xff) << 32) |
				 ((long)(id[4] & 0xff) << 24) |
				 ((long)(id[5] & 0xff) << 16));

		return t >> 16;
	}

	@Override
	public String toString() {
		return Hex.encodeHexString(id);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Id id = (Id) o;

		if (!Arrays.equals(this.id, id.id)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(id);
	}

	@Override
	public int compareTo(Id id) {
		return new BigInteger(this.id).compareTo(new BigInteger(id.id));
	}
}
