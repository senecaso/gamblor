/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.data;

import com.google.common.base.Strings;
import com.purplecritter.gamblor.bookie.exceptions.LoadException;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.model.accounts.AccountManager;
import com.purplecritter.gamblor.bookie.model.events.AbstractEventMutation;
import com.purplecritter.gamblor.bookie.model.events.Event;
import com.purplecritter.gamblor.bookie.model.events.EventId;
import com.purplecritter.gamblor.bookie.model.events.EventManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MockEventManager implements EventManager {
	private final AccountManager manager;
	private final Map<EventId, Event> events = new HashMap<>();

	private class MockEventMutation extends AbstractEventMutation {
		public MockEventMutation(Event event) {
			super(event);
		}
	}

	public MockEventManager(AccountManager manager) {
		this.manager = manager;
	}

	@Override
	public Event create(String game) throws StoreException {
		Event event = new Event(manager, this, EventId.randomEventId(), game);
		events.put(event.getId(), event);

		return event;
	}

	@Override
	public Event get(EventId id) throws LoadException {
		return events.get(id);
	}

	@Override
	public List<Event> get(String game) throws LoadException {
		if (Strings.isNullOrEmpty(game)) {
			throw new IllegalArgumentException("game is required");
		}

		List<Event> gameEvents = new ArrayList<>();
		for (Event e : events.values()) {
			if (e.getGame().equals(game)) {
				gameEvents.add(e);
			}
		}

		return gameEvents;
	}

	@Override
	public Mutation mutate(Event event) {
		return new MockEventMutation(event);
	}
}
