/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.model.accounts;

import com.google.gson.JsonObject;
import com.purplecritter.gamblor.bookie.Util;
import com.purplecritter.gamblor.bookie.exceptions.AccountClosedException;
import com.purplecritter.gamblor.bookie.exceptions.InsufficientFundsException;
import com.purplecritter.gamblor.bookie.exceptions.PaymentException;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.model.TransactionDetails;
import com.purplecritter.gamblor.bookie.model.Unique;
import com.purplecritter.gamblor.bookie.model.bets.Bet;
import com.purplecritter.gamblor.bookie.model.bets.BetId;
import com.purplecritter.gamblor.bookie.model.cashouts.Txn;
import com.purplecritter.gamblor.bookie.model.deposits.Deposit;
import com.purplecritter.gamblor.bookie.model.payouts.Payout;
import com.purplecritter.gamblor.bookie.model.payouts.PayoutId;
import com.purplecritter.gamblor.bookie.processor.PaymentProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Account implements Unique {
	private static final Logger logger = LoggerFactory.getLogger(Account.class);

	private final PaymentProcessor processor;
	private final AccountManager accountManager;

	private final AccountId accountId;
	private final String depositAddress;
	private final String returnAddress;

	protected Set<Deposit> deposits = new HashSet<>();
	protected Map<BetId, Bet> bets = new HashMap<>();
	protected Map<BetId, Payout> payouts = new HashMap<>();
	protected boolean cashingOut = false;
	protected Txn txn = null;

	private boolean doubleSpend = false;


	public Account(PaymentProcessor processor, AccountManager accountManager, AccountId accountId, String depositAddress, String returnAddress) {
		if (processor == null) {
			throw new IllegalArgumentException("processor is required");
		}

		if (accountManager == null) {
			throw new IllegalArgumentException("accountManager is required");
		}

		if (accountId == null) {
			throw new IllegalArgumentException("accountId is required");
		}

		if (depositAddress == null || depositAddress.trim().isEmpty()) {
			throw new IllegalArgumentException("depositAddress is required");
		}

		if (returnAddress == null || returnAddress.trim().isEmpty()) {
			throw new IllegalArgumentException("returnAddress is required");
		}

		this.processor = processor;

		this.accountManager = accountManager;

		this.accountId = accountId;
		this.depositAddress = depositAddress;
		this.returnAddress = returnAddress;

		processor.setDepositListener(depositAddress, new PaymentProcessor.DepositListener(accountId) {
			@Override
			public void onDepositReceived(String txid) {
				if (txid == null || txid.trim().isEmpty()) {
					throw new IllegalArgumentException("invalid txid");
				}

				try {
					Account account = Account.this.accountManager.get(accountId);
					if (account == null) {
						// if we don't know about this account anymore, stop processing its deposits
						Account.this.processor.removeDepositListener(Account.this.depositAddress);

						// TODO: figure out how to return this deposit to sender, but only if this wasn't used in the (now) unknown account.  Shit.
					} else {
						TransactionDetails details = Account.this.processor.getTransactionDetails(txid);
						Deposit deposit = new Deposit(txid, details.getAmount(Account.this.depositAddress), details.getConfirmations() >= Util.calculateMinConfirmations(details.getAmount(Account.this.depositAddress)), details.getConfirmations() < 0);

						if (!deposits.contains(deposit)) {
							AccountManager.Mutation m = Account.this.accountManager.mutate(Account.this);
							m.addDeposit(deposit);
							m.apply();

							logger.info("Received deposit of {} for account {}", deposit.getAmount(), getId());
						}
					}
				} catch (StoreException e) {
					logger.error("Unable to add deposit to account {} from transaction {}.  The user must manually be reimbursed", new Object[] {Account.this.accountId, txid, e});
				}
			}
		});
	}

	public synchronized Set<Deposit> getDeposits() {
		return Collections.unmodifiableSet(deposits);
	}

	public synchronized Map<BetId, Bet> getBets() {
		return Collections.unmodifiableMap(bets);
	}

	public synchronized Map<BetId, Payout> getPayouts() {
		return Collections.unmodifiableMap(payouts);
	}

	public synchronized boolean isCashingOut() {
		return cashingOut;
	}

	public synchronized Txn getTxn() {
		return txn;
	}

	public synchronized void setDeposits(Set<Deposit> deposits) {
		this.deposits = deposits;
	}

	public synchronized void setBets(Map<BetId, Bet> bets) {
		this.bets = bets;
	}

	public synchronized void setPayouts(Map<BetId, Payout> payouts) {
		this.payouts = payouts;
	}

	public synchronized void setCashingOut(boolean cashingOut) {
		this.cashingOut = cashingOut;
	}

	public synchronized void setTxn(Txn txn) {
		this.txn = txn;
	}

	public synchronized boolean isDoubleSpend() {
		if (doubleSpend) {
			return doubleSpend;
		}

		for (Deposit deposit : deposits) {
			if (deposit.isDoubleSpend()) {
				doubleSpend = true;
				return true;
			}
		}

		return false;
	}

	public AccountId getId() {
		return accountId;
	}

	public String getDepositAddress() {
		return depositAddress;
	}

	public String getReturnAddress() {
		return returnAddress;
	}

	public synchronized BigDecimal getBalance() {
		BigDecimal balance = BigDecimal.ZERO;
		if (!isDoubleSpend() && getTxn() == null) {
			for (Deposit deposit : deposits) {
				if (!deposit.isDoubleSpend()) {
					balance = balance.add(deposit.getAmount());
				}
			}

			for (Payout payout : payouts.values()) {
				balance = balance.add(payout.getPayout());
			}
		}

		return balance;
	}

	public synchronized boolean isConfirmed() {
		boolean isConfirmed = true;
		for (Deposit deposit : deposits) {
			if (!deposit.isConfirmed()) {
				isConfirmed = false;
				break;
			}
		}

		return isConfirmed;
	}

	private synchronized boolean allBetsSettled() {
		Set<BetId> bets = new HashSet<>(getBets().keySet());
		bets.removeAll(getPayouts().keySet());

		return bets.isEmpty();
	}

	public synchronized void attemptToProcessCashOut() throws StoreException, PaymentException {
		if (cashingOut && getTxn() == null && ((isConfirmed() && allBetsSettled()) || isDoubleSpend())) {
			BigDecimal balance = getBalance();
			logger.info("cashing out a balance of {} from account {} to {}", new Object[] {balance, accountId, returnAddress});

			if (balance.compareTo(BigDecimal.ZERO) > 0) {
				Txn txn = processor.prepareTransaction(returnAddress, balance);
				accountManager.mutate(this).setTransaction(txn).apply();

				processor.submitTransaction(txn);
				logger.info("cashed out account {} for {} with transaction {}", new Object[]{accountId, balance, txn.getId()});
			}
		}
	}

	public synchronized void cashOut() throws StoreException, PaymentException {
		logger.info("cash-out requested for account {}", accountId);

		if (!cashingOut) {
			accountManager.mutate(this).setCashingOut().apply();
			attemptToProcessCashOut();
		}
	}

	public synchronized BetId placeBet(BigDecimal stake, JsonObject outcome) throws InsufficientFundsException, StoreException, AccountClosedException {
		if (stake == null || stake.compareTo(BigDecimal.ZERO) <= 0) {
			throw new IllegalArgumentException("stake must be > 0");
		}

		if (outcome == null) {
			throw new IllegalArgumentException("outcome is required");
		}

		if (isDoubleSpend()) {
			throw new AccountClosedException("caught cheating with a double-spend");
		}

		if (isCashingOut()) {
			throw new AccountClosedException("request to cash out is being processed");
		}

		BigDecimal balance = getBalance();
		if (balance.subtract(stake).compareTo(BigDecimal.ZERO) < 0) {
			logger.info("Attempt to place bet of {} on account {} failed due to insufficient funds.  Current balance is {}", new Object[]{stake, accountId, balance});
			throw new InsufficientFundsException();
		}

		Bet bet = new Bet(BetId.randomBetId(), stake, outcome);
		accountManager.mutate(this).addBet(bet).apply();

		logger.info("placed bet {} for {}", bet.getId(), bet.getStake());
		return bet.getId();
	}

	public synchronized void settleBet(BetId betId, BigDecimal payout) throws StoreException, PaymentException {
		if (betId == null) {
			throw new IllegalArgumentException("betId is required");
		}

		if (payout == null || payout.compareTo(BigDecimal.ZERO) < 0) {
			throw new IllegalArgumentException("payout must be >= 0");
		}

		if (isDoubleSpend()) {
			payout = BigDecimal.ZERO;
		}

		if (payouts.containsKey(betId)) {
			logger.info("Attempting to settle a bet which has already been settled: {}", betId);
			return;
		}

		AccountManager.Mutation mutation = accountManager.mutate(this);
		mutation.addPayout(new Payout(PayoutId.randomPayoutId(), betId, payout));
		mutation.apply();

		if (isCashingOut()) {
			attemptToProcessCashOut();
		}
	}

	@Override
	public String toString() {
		return accountId + " : " + getBalance();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Account account = (Account) o;

		if (!accountId.equals(account.accountId)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		return accountId.hashCode();
	}
}
