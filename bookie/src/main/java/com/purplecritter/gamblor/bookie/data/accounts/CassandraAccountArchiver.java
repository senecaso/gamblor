/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.data.accounts;

import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.model.ColumnFamily;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.model.accounts.Account;
import com.purplecritter.gamblor.bookie.model.accounts.AccountManager;
import com.purplecritter.gamblor.bookie.model.bets.Bet;
import com.purplecritter.gamblor.bookie.model.cashouts.Txn;
import com.purplecritter.gamblor.bookie.model.deposits.Deposit;
import com.purplecritter.gamblor.bookie.model.payouts.Payout;

public class CassandraAccountArchiver extends CassandraAccountMutation {
	public CassandraAccountArchiver(Account account, Keyspace ks, ColumnFamily<String, AccountComposite> cf) {
		super(account, ks, cf);
	}

	@Override
	public AccountManager.Mutation addBet(Bet bet) {
		throw new UnsupportedOperationException("cannot mutate an archived account");
	}

	@Override
	public AccountManager.Mutation addPayout(Payout payout) {
		throw new UnsupportedOperationException("cannot mutate an archived account");
	}

	@Override
	public AccountManager.Mutation addDeposit(Deposit deposit) {
		throw new UnsupportedOperationException("cannot mutate an archived account");
	}

	@Override
	public AccountManager.Mutation setCashingOut() {
		throw new UnsupportedOperationException("cannot mutate an archived account");
	}

	@Override
	public AccountManager.Mutation setTransaction(Txn txn) {
		throw new UnsupportedOperationException("cannot mutate an archived account");
	}

	@Override
	public void apply() throws StoreException {
		clm.putColumn(new AccountComposite(AccountConstants.OTHER, AccountConstants.DEPOSIT_ADDRESS), account.getDepositAddress());
		clm.putColumn(new AccountComposite(AccountConstants.OTHER, AccountConstants.RETURN_ADDRESS), account.getReturnAddress());

		for (Deposit deposit : account.getDeposits()) {
			super.addDeposit(deposit);
		}

		for (Bet bet : account.getBets().values()) {
			super.addBet(bet);
		}

		for (Payout payout : account.getPayouts().values()) {
			super.addPayout(payout);
		}

		if (account.isCashingOut()) {
			super.setCashingOut();
		}

		super.apply();
	}
}
