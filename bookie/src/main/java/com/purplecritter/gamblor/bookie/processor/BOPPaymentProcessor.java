/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.processor;

import com.bitsofproof.supernode.api.AccountListener;
import com.bitsofproof.supernode.api.AccountManager;
import com.bitsofproof.supernode.api.AddressConverter;
import com.bitsofproof.supernode.api.BCSAPIException;
import com.bitsofproof.supernode.api.Block;
import com.bitsofproof.supernode.api.FileWallet;
import com.bitsofproof.supernode.api.JMSServerConnector;
import com.bitsofproof.supernode.api.Transaction;
import com.bitsofproof.supernode.api.TransactionListener;
import com.bitsofproof.supernode.api.TransactionOutput;
import com.bitsofproof.supernode.api.TrunkListener;
import com.bitsofproof.supernode.common.Hash;
import com.bitsofproof.supernode.common.ValidationException;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.purplecritter.gamblor.bookie.data.MiscStorage;
import com.purplecritter.gamblor.bookie.exceptions.PaymentException;
import com.purplecritter.gamblor.bookie.model.TransactionDetails;
import com.purplecritter.gamblor.bookie.model.cashouts.Txn;
import org.fusesource.stomp.jms.StompJmsConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class BOPPaymentProcessor implements PaymentProcessor {
	private static final Logger logger = LoggerFactory.getLogger(BOPPaymentProcessor.class);

	private static final String ACCOUNT = "hotAccount";
	private static final int LOOK_AHEAD = 200; // if this is too small, the wallet will be missing spendable outputs
	private static final String LAST_PROCESSED_BLOCK_HEIGHT = "lastProcessedBlockHeight";

	private final JMSServerConnector api;
	private final AccountManager btcAccountManager;
	private final int addressFlag;

	private final Map<String, DepositListener> depositListeners = new ConcurrentHashMap<>();
	private final ConcurrentHashMap<BlockListener, Boolean> confirmationListeners = new ConcurrentHashMap<>();
	private final CurrentBlockListener currentBlockListener;
	private final TxnListener txnListener;

	private final FileWallet wallet;
	private final String walletPassword;

	private final MiscStorage miscStorage;
	private final Integer lastProcessedBlockHeight; // the last processed block height from the LAST time this class was run; ie before we crashed

	private final class CurrentBlockListener implements TrunkListener {
		private volatile int currentHeight = 0;

		public CurrentBlockListener() throws BCSAPIException {
			// prime the current height with the height of the currently known head block
			Block currentBlockHeader = api.getBlockHeader(Hash.ZERO_HASH_STRING);
			if (currentBlockHeader != null) {
				currentHeight = currentBlockHeader.getHeight();
			}
		}

		@Override
		public void trunkUpdate(List<Block> removed, List<Block> added) {
			int newHeight = 0;
			for (Block b : added) {
				newHeight = Math.max(newHeight, b.getHeight());
			}

			currentHeight = newHeight;

			// fire off the confirmation listeners, since each of them -should- have received one new confirmation right now
			for (BlockListener listener : confirmationListeners.keySet()) {
				listener.onBlockFound();
			}

			try {
				miscStorage.put(LAST_PROCESSED_BLOCK_HEIGHT, currentHeight);
			} catch (ConnectionException e) {
				logger.info("Unable to persist last processed block height.  Ignoring and continuing ...", e);
			}
		}
	}

	private class TxnListener implements AccountListener, TransactionListener {

		@Override
		public void accountChanged(AccountManager accountManager, Transaction t) {
			process(t);
		}

		@Override
		public void process(Transaction t) {
			logger.info("processing incoming txn: {}", t.getHash());

			boolean found = false;
			for (TransactionOutput output : t.getOutputs()) {
				DepositListener depositListener = depositListeners.get(AddressConverter.toSatoshiStyle(output.getOutputAddress(), addressFlag));
				if (depositListener != null) {
					depositListener.onDepositReceived(t.getHash());
					found = true;
				}
			}

			if (!found) {
				// nothing was listening for this deposit, so it was probably an accident.  Return it to the sender
				// TODO: figure out how to send these exact outputs back to the sender; less the transaction fee
			}
		}
	}

	public BOPPaymentProcessor(String brokerURI, String username, String password, File walletFile, Keyspace ks) throws IOException, ValidationException, BCSAPIException, ConnectionException {
		StompJmsConnectionFactory connectionFactory = new StompJmsConnectionFactory();
		connectionFactory.setBrokerURI(brokerURI);
		connectionFactory.setUsername(username);
		connectionFactory.setPassword(password);

		api = new JMSServerConnector();
		api.setConnectionFactory(connectionFactory);
		api.setClientId(UUID.randomUUID().toString());
		api.init();

		addressFlag = api.isProduction() ? 0 : 111;

		FileWallet wallet = new FileWallet(walletFile.getAbsolutePath());
		if (!wallet.exists()) {
			wallet.init(password);
			wallet.persist();
		} else {
			wallet = FileWallet.read(walletFile.getAbsolutePath());
			wallet.sync(api, LOOK_AHEAD);
		}

		this.wallet = wallet;
		this.walletPassword = password;

		AccountManager mgr = wallet.getAccountManager(ACCOUNT);
		if (mgr == null) {
			wallet.unlock(password);

			try {
				mgr = wallet.createAccountManager(ACCOUNT);
			} finally {
				wallet.lock();
			}

			wallet.persist();
		}

		txnListener = new TxnListener();

		btcAccountManager = mgr;
		api.registerTransactionListener(btcAccountManager);
		btcAccountManager.addAccountListener(txnListener);

		String msg = "\nwallet:\n"
				+ "\tbalance: {}\n"
				+ "\tconfirmed: {}\n"
				+ "\tsending: {}\n"
				+ "\treceiving: {}\n";
		logger.info(msg, new Object[]{BigDecimal.valueOf(mgr.getBalance()).movePointLeft(8), BigDecimal.valueOf(mgr.getConfirmed()).movePointLeft(8), BigDecimal.valueOf(mgr.getSending()).movePointLeft(8), BigDecimal.valueOf(mgr.getReceiving()).movePointLeft(8)});

		miscStorage = new MiscStorage(ks);
		lastProcessedBlockHeight = miscStorage.get(LAST_PROCESSED_BLOCK_HEIGHT);

		currentBlockListener = new CurrentBlockListener();
		api.registerTrunkListener(currentBlockListener);
	}

	@Override
	public Txn prepareTransaction(String destAddress, BigDecimal amount) throws PaymentException {
		try {
			wallet.unlock(walletPassword);
			Transaction t = btcAccountManager.pay(AddressConverter.fromSatoshiStyle(destAddress, addressFlag), amount.movePointRight(8).longValue());
			return new Txn(t.getHash(), t.toWireDump());
		} catch (ValidationException e) {
			logger.warn("Attempting to make a payment of {} to an invalid address: {}", new Object[]{amount, destAddress, e});
			throw new PaymentException(e);
		} finally {
			wallet.lock();
		}
	}

	@Override
	public void submitTransaction(Txn txn) throws PaymentException {
		try {
			api.sendTransaction(Transaction.fromWireDump(txn.getTxn()));
		} catch (BCSAPIException e) {
			logger.warn("Failed to submit transaction: {}", txn, e);
			throw new PaymentException(e);
		}
	}

	@Override
	public String createAddress() {
		try {
			return AddressConverter.toSatoshiStyle(btcAccountManager.getNextKey().getAddress(), addressFlag);
		} catch (ValidationException e) {
			logger.warn("Failed to create new address", e);
			throw new RuntimeException("Failed to validate an address we generated ourselves!", e);
		}
	}

	@Override
	public void setDepositListener(String depositAddress, DepositListener listener) {
		if (depositAddress == null || depositAddress.trim().isEmpty()) {
			throw new IllegalArgumentException("invalid depositAddress");
		}

		if (listener == null) {
			throw new IllegalArgumentException("invalid deposit listener specified");
		}

		depositListeners.put(depositAddress, listener);
	}

	@Override
	public void removeDepositListener(String depositAddress) {
		depositListeners.remove(depositAddress);
	}

	@Override
	public boolean isValidAddress(String address) {
		try {
			AddressConverter.fromSatoshiStyle(address, addressFlag);
			return true;
		} catch (ValidationException e) {
			return false;
		}
	}

	@Override
	public TransactionDetails getTransactionDetails(String txid) {
		try {
			Transaction t = api.getTransaction(txid);
			if (t != null) {
				int confirmations;
				if (t.isDoubleSpend()) {
					confirmations = -1;
				} else {
					confirmations = t.getHeight() != 0 ? currentBlockListener.currentHeight - t.getHeight() + 1: 0;
				}

				Map<String, BigDecimal> outputAmounts = new HashMap<>();
				for (TransactionOutput output : t.getOutputs()) {
					String address = AddressConverter.toSatoshiStyle(output.getOutputAddress(), addressFlag);

					BigDecimal amount = outputAmounts.get(address);
					if (amount == null) {
						amount = BigDecimal.valueOf(output.getValue()).movePointLeft(8);
					} else {
						amount = amount.add(BigDecimal.valueOf(output.getValue()).movePointLeft(8));
					}

					outputAmounts.put(address, amount);
				}

				return new TransactionDetails(txid, outputAmounts, confirmations, t.getHeight());
			}

			return null;
		} catch (BCSAPIException e) {
			logger.error("Failed to connect to BOP", e);
			throw new RuntimeException("Unable to determine number of confirmations", e);
		}
	}

	@Override
	public void processMissedTransactions() {
		// if this is the first time we have ever run, there wont be a last processed block, so there is nothing to reprocess now
		if (lastProcessedBlockHeight != null) {
			try {
				wallet.unlock(walletPassword);
				int lookAhead = LOOK_AHEAD;
				api.scanTransactions(wallet.getMaster(), lookAhead, lastProcessedBlockHeight, txnListener);
			} catch (ValidationException | BCSAPIException e) {
				logger.error("Unable to process missed transactions", e);
				throw new RuntimeException("Unable process missed transactions", e);
			} finally {
				wallet.lock();
			}
		}
	}

	@Override
	public void registerBlockListener(BlockListener listener) {
		confirmationListeners.put(listener, Boolean.TRUE);
	}

	@Override
	public void unregisterBlockListener(BlockListener listener) {
		confirmationListeners.remove(listener);
	}
}
