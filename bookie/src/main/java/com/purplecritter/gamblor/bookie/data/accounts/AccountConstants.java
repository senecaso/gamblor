/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.data.accounts;

public class AccountConstants {
	public static final String DEPOSIT_ADDRESS = "depositAddress";
	public static final String RETURN_ADDRESS = "returnAddress";
	public static final String CASH_OUT = "cashOut";
	public static final String TXN = "txn";

	public static final int DEPOSIT = 0;
	public static final int BET = 1;
	public static final int PAYOUT = 2;
	public static final int OTHER = Integer.MAX_VALUE;

	public static final String STAKE = "stake";
	public static final String OUTCOME = "outcome";

	public static final String PAYOUT_AMT = "amount";
	public static final String PAYOUT_BET_ID = "betId";
}
