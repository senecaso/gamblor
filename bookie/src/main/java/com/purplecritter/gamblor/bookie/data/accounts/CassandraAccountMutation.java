/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.data.accounts;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.netflix.astyanax.ColumnListMutation;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.MutationBatch;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.model.ColumnFamily;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.model.accounts.AbstractAccountMutation;
import com.purplecritter.gamblor.bookie.model.accounts.Account;
import com.purplecritter.gamblor.bookie.model.accounts.AccountManager;
import com.purplecritter.gamblor.bookie.model.bets.Bet;
import com.purplecritter.gamblor.bookie.model.cashouts.Txn;
import com.purplecritter.gamblor.bookie.model.deposits.Deposit;
import com.purplecritter.gamblor.bookie.model.payouts.Payout;

public class CassandraAccountMutation extends AbstractAccountMutation {
	private static final byte[] EMPTY_VALUE = new byte[0];

	protected final MutationBatch m;
	protected final ColumnListMutation<AccountComposite> clm;

	public CassandraAccountMutation(Account account, Keyspace ks, ColumnFamily<String, AccountComposite> cf) {
		super(account);

		m = ks.prepareMutationBatch();
		clm = m.withRow(cf, account.getId().toString());
	}

	@Override
	public AccountManager.Mutation addBet(Bet bet) {
		if (!bets.containsKey(bet.getId())) {
			JsonObject json = new JsonObject();
			json.add(AccountConstants.STAKE, new JsonPrimitive(bet.getStake()));
			json.add(AccountConstants.OUTCOME, bet.getOutcome());

			clm.putColumn(new AccountComposite(AccountConstants.BET, bet.getId().toString()), json.toString());
		}

		super.addBet(bet);
		return this;
	}

	@Override
	public AccountManager.Mutation addPayout(Payout payout) {
		if (!payouts.containsKey(payout.getBetId())) {
			JsonObject json = new JsonObject();
			json.add(AccountConstants.PAYOUT_BET_ID, new JsonPrimitive(payout.getBetId().toString()));
			json.add(AccountConstants.PAYOUT_AMT, new JsonPrimitive(payout.getPayout()));

			clm.putColumn(new AccountComposite(AccountConstants.PAYOUT, payout.getId().toString()), json.toString());
		}

		super.addPayout(payout);
		return this;
	}

	@Override
	public AccountManager.Mutation addDeposit(Deposit deposit) {
		if (!deposits.contains(deposit)) {
			clm.putColumn(new AccountComposite(AccountConstants.DEPOSIT, deposit.getTxid()), EMPTY_VALUE);
		}

		super.addDeposit(deposit);
		return this;
	}

	@Override
	public AccountManager.Mutation setCashingOut() {
		if (!cashingOut) {
			super.setCashingOut();
			clm.putColumn(new AccountComposite(AccountConstants.OTHER, AccountConstants.CASH_OUT), cashingOut);
		}

		return this;
	}

	@Override
	public AccountManager.Mutation setTransaction(Txn txn) {
		if (!txn.equals(this.txn)) {
			clm.putColumn(new AccountComposite(AccountConstants.OTHER, AccountConstants.TXN), txn.getTxn());
		}

		super.setTransaction(txn);
		return this;
	}

	@Override
	public void apply() throws StoreException {
		try {
			m.execute();

			super.apply();
		} catch (ConnectionException e) {
			throw new StoreException(e);
		}
	}
}
