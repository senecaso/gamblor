/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.model.accounts;

import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.model.bets.Bet;
import com.purplecritter.gamblor.bookie.model.bets.BetId;
import com.purplecritter.gamblor.bookie.model.cashouts.Txn;
import com.purplecritter.gamblor.bookie.model.deposits.Deposit;
import com.purplecritter.gamblor.bookie.model.payouts.Payout;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class AbstractAccountMutation implements AccountManager.Mutation {
	protected Set<Deposit> deposits;
	protected Map<BetId, Bet> bets;
	protected HashMap<BetId, Payout> payouts;
	protected boolean cashingOut;
	protected Txn txn;

	protected final Account account;

	public AbstractAccountMutation(Account account) {
		this.account = account;
		this.deposits = new HashSet<>(account.getDeposits());
		this.bets = new HashMap<>(account.getBets());
		this.payouts = new HashMap<>(account.getPayouts());
		this.cashingOut = account.isCashingOut();
		this.txn = account.getTxn();
	}

	@Override
	public AccountManager.Mutation addBet(Bet bet) {
		bets.put(bet.getId(), bet);
		return this;
	}

	@Override
	public AccountManager.Mutation addPayout(Payout payout) {
		payouts.put(payout.getBetId(), payout);
		return this;
	}

	@Override
	public AccountManager.Mutation addDeposit(Deposit deposit) {
		deposits.add(deposit);
		return this;
	}

	@Override
	public AccountManager.Mutation setCashingOut() {
		this.cashingOut = true;
		return this;
	}

	@Override
	public AccountManager.Mutation setDoubleSpend(String txid) {
		Deposit deposit = null;
		for (Deposit d : deposits) {
			if (d.getTxid().equals(txid)) {
				deposit = d;
			}
		}

		if (deposit != null) {
			deposits.remove(deposit);
			deposits.add(new Deposit(deposit.getTxid(), deposit.getAmount(), deposit.isConfirmed(), true));
		}

		return this;
	}

	@Override
	public AccountManager.Mutation setConfirmed(String txid) {
		Deposit deposit = null;
		for (Deposit d : deposits) {
			if (d.getTxid().equals(txid)) {
				deposit = d;
			}
		}

		if (deposit != null) {
			deposits.remove(deposit);
			deposits.add(new Deposit(deposit.getTxid(), deposit.getAmount(), true, deposit.isDoubleSpend()));
		}

		return this;
	}

	@Override
	public AccountManager.Mutation setTransaction(Txn txn) {
		if (this.txn != null) {
			throw new IllegalArgumentException("Cannot assign a transaction when there is already a pending transaction");
		}

		this.txn = txn;
		return this;
	}

	@Override
	public void apply() throws StoreException {
		account.setDeposits(deposits);
		account.setBets(bets);
		account.setPayouts(payouts);
		account.setCashingOut(cashingOut);
		account.setTxn(txn);
	}
}
