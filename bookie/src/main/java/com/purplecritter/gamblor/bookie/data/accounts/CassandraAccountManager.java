/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.data.accounts;

import com.bitsofproof.supernode.api.Transaction;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.netflix.astyanax.ExceptionCallback;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.MutationBatch;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.model.Column;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.model.ColumnList;
import com.netflix.astyanax.model.Row;
import com.netflix.astyanax.model.Rows;
import com.netflix.astyanax.serializers.AnnotatedCompositeSerializer;
import com.netflix.astyanax.serializers.StringSerializer;
import com.purplecritter.gamblor.bookie.Util;
import com.purplecritter.gamblor.bookie.exceptions.PaymentException;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.model.TransactionDetails;
import com.purplecritter.gamblor.bookie.model.accounts.AbstractAccountManager;
import com.purplecritter.gamblor.bookie.model.accounts.Account;
import com.purplecritter.gamblor.bookie.model.accounts.AccountId;
import com.purplecritter.gamblor.bookie.model.bets.Bet;
import com.purplecritter.gamblor.bookie.model.bets.BetId;
import com.purplecritter.gamblor.bookie.model.cashouts.Txn;
import com.purplecritter.gamblor.bookie.model.deposits.Deposit;
import com.purplecritter.gamblor.bookie.model.payouts.Payout;
import com.purplecritter.gamblor.bookie.model.payouts.PayoutId;
import com.purplecritter.gamblor.bookie.processor.PaymentProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CassandraAccountManager extends AbstractAccountManager {
	private static final Logger logger = LoggerFactory.getLogger(CassandraAccountManager.class);

	private static final String CF = "accounts";
	private static final String ARCHIVE_CF = "accountsArchive";
	private static final long MAX_IDLE_TIME = TimeUnit.MINUTES.toMillis(30);
	private static final long POLL_TIME = 30;
	private static final TimeUnit POLL_TIME_UNIT = TimeUnit.SECONDS;

	private static AnnotatedCompositeSerializer<AccountComposite> accountSerializer = new AnnotatedCompositeSerializer<>(AccountComposite.class);


	private final PaymentProcessor processor;
	private final Keyspace ks;
	private final ColumnFamily<String, AccountComposite> cf;
	private final ColumnFamily<String, AccountComposite> archiveCF;

	private final Map<AccountId, AccountInfo> accounts = new ConcurrentHashMap<>();

	private static class AccountInfo {
		private final Account account;
		private long accessTime;

		public AccountInfo(Account account) {
			this.account = account;
			this.accessTime = System.currentTimeMillis();
		}
	}

	public CassandraAccountManager(PaymentProcessor processor, Keyspace ks) throws ConnectionException {
		super(processor);

		if (ks == null) {
			throw new IllegalArgumentException("ks is required");
		}

		this.processor = processor;

		this.ks = ks;
		this.cf = new ColumnFamily<>(CF, StringSerializer.get(), accountSerializer);
		this.archiveCF = new ColumnFamily<>(ARCHIVE_CF, StringSerializer.get(), accountSerializer);

		if (ks.describeKeyspace().getColumnFamily(CF) == null) {
			ks.createColumnFamily(cf, ImmutableMap.<String, Object>builder()
					.put("comparator_type", "CompositeType(LongType, UTF8Type)")
					.build());
		}

		if (ks.describeKeyspace().getColumnFamily(ARCHIVE_CF) == null) {
			ks.createColumnFamily(archiveCF, ImmutableMap.<String, Object>builder()
					.put("comparator_type", "CompositeType(LongType, UTF8Type)")
					.build());
		}

		// load ALL accounts and auto-cashout ones which have been inactive for too long
		Rows<String, AccountComposite> rows;
		try {
			rows = ks.prepareQuery(cf)
					.getAllRows()
					.setRowLimit(50)
					.setExceptionCallback(new ExceptionCallback() {
						@Override
						public boolean onException(ConnectionException e) {
							logger.error("Unable to connect to cassandra", e);
							return false; // don't bother retrying
						}
					})
					.execute().getResult();
		} catch (ConnectionException e) {
			logger.error("Unable to connect to cassandra", e);
			throw new RuntimeException(e);
		}

		// load all accounts from storage to ensure that we don't forget to cash-out any idle accounts
		int count = 0;
		for (Row<String, AccountComposite> row : rows) {
			Account account = loadFromRow(new AccountId(row.getKey()), row.getColumns());
			if (account != null) {
				count++;
				accounts.put(account.getId(), new AccountInfo(account));

				if (account.getTxn() != null) {
					try {
						processor.submitTransaction(account.getTxn());
					} catch (PaymentException e) {
						logger.warn("Failed to submit cash-out transaction on account {}", account.getId(), e);
					}
				}
			}
		}
		logger.info("Loaded {} accounts from storage", count);

		// reprocess all transactions since the last successful block to ensure we didn't miss any deposits
		processor.processMissedTransactions();

		// kick off the account cleaner to cash-out accounts which have been idle for too long
		ScheduledExecutorService accountCleaner = Executors.newSingleThreadScheduledExecutor();
		accountCleaner.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				try {
					int count = 0;

					logger.debug("Starting to clean accounts ...");
					for (Map.Entry<AccountId, AccountInfo> entry : accounts.entrySet()) {
						AccountInfo info = entry.getValue();
						if (System.currentTimeMillis() - info.accessTime > MAX_IDLE_TIME) {
							try {
								logger.debug("Cleaning account {} ...", info.account.getId());

								if (info.account.isCashingOut()) {
									Txn txn = info.account.getTxn();
									TransactionDetails details = txn == null ? null : CassandraAccountManager.this.processor.getTransactionDetails(txn.getId());
									if (txn != null && details != null && details.getConfirmations() > 2) {
										archive(info.account);
										count++;
									} else if (txn == null && info.account.getBalance().compareTo(BigDecimal.ZERO) <= 0) {
										// TODO: what if they dont have enough to cover the transaction fee?
										archive(info.account);
										count++;
									} else if (txn != null && details == null) {
										logger.warn("Account {} has an expected cashout txn {}, but the txn cannot be found in the processor", info.account.getId(), txn.getId());
									} else {
										long idleMinutes = TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - info.accessTime);
										if (idleMinutes >= 60) {
											if (info.account.getTxn() != null) {
												logger.warn("Account {} has been trying to cash-out for ~{} mins", entry.getKey(), idleMinutes);
											} else if (info.account.getBets().size() != info.account.getPayouts().size()) {
												logger.warn("Account {} has been waiting for ~{} mins for settlement on bets before it can cash-out", entry.getKey(), idleMinutes);
											} else if (!info.account.isConfirmed()) {
												logger.warn("Account {} has been waiting for ~{} mins for deposit confirmation", entry.getKey(), idleMinutes);
											} else {
												logger.warn("Account {} has been trying to cash-out for ~{} mins for an unknown reason", entry.getKey(), idleMinutes);
											}
										}

										info.account.attemptToProcessCashOut();
									}
								} else {
									logger.info("forcing cashout of account {} due to inactivity", info.account.getId());
									info.account.cashOut();
								}
							} catch (StoreException | PaymentException e) {
								logger.info("Cleaner failed to process account {}", info.account.getId(), e);
							}
						}
					}

					if (count > 0) {
						logger.info("Archived {} accounts", count);
					}

					logger.debug("Finished cleaning accounts");
				} catch (Exception e) {
					logger.warn("Uncaught exception while cleaning accounts", e);
				}
			}
		}, POLL_TIME, POLL_TIME, POLL_TIME_UNIT);
	}

	private Account loadFromRow(AccountId accountId, ColumnList<AccountComposite> columns) {
		if (!columns.isEmpty()) {
			String depositAddress;
			String returnAddress = null;
			Set<Deposit> deposits = new HashSet<>();
			Map<BetId, Bet> bets = new HashMap<>();
			Map<BetId, Payout> payouts = new HashMap<>();
			Txn txn = null;
			boolean cashingOut = false;

			Column<AccountComposite> depositAddressCol = columns.getColumnByName(new AccountComposite(AccountConstants.OTHER, AccountConstants.DEPOSIT_ADDRESS));
			if (depositAddressCol == null) {
				logger.warn("Account with no depositAddress found.  Ignoring and continuing... {}", accountId);
				return null;
			}

			depositAddress = depositAddressCol.getStringValue();

			for (Column<AccountComposite> column : columns) {
				AccountComposite composite = column.getName();
				if (composite.type == AccountConstants.OTHER) {
					if (composite.id.equals(AccountConstants.RETURN_ADDRESS)) {
						returnAddress = column.getStringValue();
					} else if (composite.id.equals(AccountConstants.CASH_OUT)) {
						cashingOut = column.getBooleanValue();
					} else if (composite.id.equals(AccountConstants.TXN)) {
						Transaction t = Transaction.fromWireDump(column.getStringValue());
						txn = new Txn(t.getHash(), column.getStringValue());
					}
				} else if (composite.type == AccountConstants.DEPOSIT) {
					TransactionDetails details = processor.getTransactionDetails(composite.id);
					if (details != null) {
						boolean isConfirmed = details.getConfirmations() >= Util.calculateMinConfirmations(details.getAmount(depositAddress));
						boolean isDoubleSpend = details.getConfirmations() < 0;

						Deposit deposit = new Deposit(composite.id, details.getAmount(depositAddress), isConfirmed, isDoubleSpend);

						deposits.add(deposit);
					} else {
						logger.warn("Account {} contains a deposit {}, but the transaction cannot be found in the blockchain!  Excluding this deposit...", accountId, composite.id);
					}
				} else if (composite.type == AccountConstants.BET) {
					JsonObject json = (JsonObject) new JsonParser().parse(column.getStringValue());
					BetId id = new BetId(composite.id);
					bets.put(id, new Bet(id, json.get(AccountConstants.STAKE).getAsBigDecimal(), json.get(AccountConstants.OUTCOME).getAsJsonObject()));
				} else if (composite.type == AccountConstants.PAYOUT) {
					JsonObject json = (JsonObject) new JsonParser().parse(column.getStringValue());
					BetId betId = new BetId(json.get(AccountConstants.PAYOUT_BET_ID).getAsString());
					payouts.put(betId, new Payout(new PayoutId(composite.id), betId, json.get(AccountConstants.PAYOUT_AMT).getAsBigDecimal()));
				}
			}

			Account account = new Account(processor, CassandraAccountManager.this, accountId, depositAddress, returnAddress);
			account.setDeposits(deposits);
			account.setBets(bets);
			account.setPayouts(payouts);
			account.setCashingOut(cashingOut);

			if (txn != null) {
				account.setTxn(txn);
			}

			return account;
		}

		return null;
	}

	@Override
	public Account create(String returnAddress) throws StoreException {
		try {
			if (returnAddress == null || returnAddress.trim().isEmpty()) {
				throw new IllegalArgumentException("invalid returnAddress");
			}

			AccountId id = AccountId.randomAccountId();
			String depositAddress = processor.createAddress();

			MutationBatch m = ks.prepareMutationBatch();
			m.withRow(cf, id.toString())
					.putColumn(new AccountComposite(AccountConstants.OTHER, AccountConstants.DEPOSIT_ADDRESS), depositAddress)
					.putColumn(new AccountComposite(AccountConstants.OTHER, AccountConstants.RETURN_ADDRESS), returnAddress);
			m.execute();

			Account account = new Account(processor, this, id, depositAddress, returnAddress);
			accounts.put(id, new AccountInfo(account));

			return account;
		} catch (ConnectionException e) {
			throw new StoreException(e);
		}
	}

	@Override
	public Account get(AccountId id) {
		AccountInfo info = accounts.get(id);
		if (info != null) {
			info.accessTime = System.currentTimeMillis();
			return info.account;
		}

		return null;
	}

	@Override
	public Mutation mutate(Account account) {
		return new CassandraAccountMutation(account, ks, cf);
	}

	@Override
	protected Collection<Account> getAccounts() {
		List<Account> accounts = new ArrayList<>();
		for (AccountInfo info : this.accounts.values()) {
			accounts.add(info.account);
		}

		return accounts;
	}

	@Override
	public void remove(AccountId id) throws StoreException {
		AccountInfo info = accounts.remove(id);

		if (info != null) {
			try {
				MutationBatch m = ks.prepareMutationBatch();
				m.withRow(cf, id.toString()).delete();
				m.execute();
			} catch (ConnectionException e) {
				throw new StoreException(e);
			}
		}
	}

	private void archive(Account account) throws StoreException {
		new CassandraAccountArchiver(account, ks, archiveCF).apply();
		remove(account.getId());
	}
}
