/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.processor;

import com.purplecritter.gamblor.bookie.exceptions.PaymentException;
import com.purplecritter.gamblor.bookie.model.TransactionDetails;
import com.purplecritter.gamblor.bookie.model.cashouts.Txn;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class MockPaymentProcessor implements PaymentProcessor {
	private final Map<String, PaymentListener> listeners = new HashMap<>();
	private final Map<String, DepositListener> depositListeners = new HashMap<>();
	private final Map<String, Integer> confirmations = new HashMap<>();
	private final Map<String, TransactionDetails> txnDetails = new HashMap<>();
	private final Set<BlockListener> blockListeners = new HashSet<>();

	public static interface PaymentListener {
		public void onPayment(BigDecimal amount);
	}

	public void addListener(String destAddress, PaymentListener listener) {
		listeners.put(destAddress, listener);
	}


	@Override
	public Txn prepareTransaction(String destAddress, BigDecimal amount) throws PaymentException {
		return new Txn(UUID.randomUUID().toString(), destAddress + "," + amount);
	}

	@Override
	public void submitTransaction(Txn txn) throws PaymentException {
		String[] parts = txn.getTxn().split(",");
		PaymentListener listener = listeners.get(parts[0]);
		if (listener != null) {
			listener.onPayment(new BigDecimal(parts[1]));
		}

		Map<String, BigDecimal> amounts = new HashMap<>();
		amounts.put(parts[0], new BigDecimal(parts[1]));

		TransactionDetails details = new TransactionDetails(txn.getId(), amounts, 0, 0);
		txnDetails.put(txn.getId(), details);
	}

	@Override
	public String createAddress() {
		return "FAKEADDRESS";
	}

	@Override
	public void setDepositListener(String depositAddress, DepositListener listener) {
		if (depositAddress == null || depositAddress.trim().isEmpty()) {
			throw new IllegalArgumentException("invalid depositAddress");
		}

		if (listener == null) {
			throw new IllegalArgumentException("invalid listener");
		}

		depositListeners.put(depositAddress, listener);
	}

	@Override
	public void removeDepositListener(String depositAddress) {
		depositListeners.remove(depositAddress);
	}

	@Override
	public boolean isValidAddress(String address) {
		return true;
	}

	@Override
	public TransactionDetails getTransactionDetails(String txid) {
		return txnDetails.get(txid);
	}

	@Override
	public void processMissedTransactions() {
		for (String txid : txnDetails.keySet()) {
			for (DepositListener dl : depositListeners.values()) {
				dl.onDepositReceived(txid);
			}
		}
	}

	@Override
	public void registerBlockListener(BlockListener listener) {
		blockListeners.add(listener);
	}

	@Override
	public void unregisterBlockListener(BlockListener listener) {
		blockListeners.remove(listener);
	}

	public void deposit(String depositAddress, String txid, BigDecimal amount) {
		Map<String, BigDecimal> amounts = new HashMap<>();
		amounts.put(depositAddress, amount);

		txnDetails.put(txid, new TransactionDetails(txid, amounts, 0, 0));

		DepositListener listener = depositListeners.get(depositAddress);
		if (listener != null) {
			listener.onDepositReceived(txid);
		}
	}

	public void setConfirmations(String txid, int confirmations) {
		this.confirmations.put(txid, confirmations);
		TransactionDetails details = txnDetails.get(txid);
		txnDetails.put(txid, new TransactionDetails(txid, details.getAmounts(), confirmations, 0));

		for (BlockListener listener : blockListeners) {
			listener.onBlockFound();
		}
	}
}
