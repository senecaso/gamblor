/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.data.events;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.netflix.astyanax.ColumnListMutation;
import com.netflix.astyanax.ExceptionCallback;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.MutationBatch;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.model.Column;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.model.ColumnList;
import com.netflix.astyanax.model.Row;
import com.netflix.astyanax.model.Rows;
import com.netflix.astyanax.serializers.AnnotatedCompositeSerializer;
import com.netflix.astyanax.serializers.StringSerializer;
import com.purplecritter.gamblor.bookie.exceptions.LoadException;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.model.accounts.AccountId;
import com.purplecritter.gamblor.bookie.model.accounts.AccountManager;
import com.purplecritter.gamblor.bookie.model.bets.BetId;
import com.purplecritter.gamblor.bookie.model.events.AbstractEventMutation;
import com.purplecritter.gamblor.bookie.model.events.Event;
import com.purplecritter.gamblor.bookie.model.events.EventId;
import com.purplecritter.gamblor.bookie.model.events.EventManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CassandraEventManager implements EventManager {
	private static final Logger logger = LoggerFactory.getLogger(CassandraEventManager.class);

	private static final String CF = "events";

	private static final String GAME = "game";
	private static final String IS_CLOSED = "isClosed";

	private static final long POLL_TIME = 30;
	private static final TimeUnit POLL_TIME_UNIT = TimeUnit.SECONDS;
	private static final byte[] EMPTY = new byte[0];

	private static AnnotatedCompositeSerializer<EventComposite> eventSerializer = new AnnotatedCompositeSerializer<>(EventComposite.class);

	private class EventMutation extends AbstractEventMutation {
		private final MutationBatch m;
		private final ColumnListMutation<EventComposite> clm;

		public EventMutation(Event event) {
			super(event);

			m = ks.prepareMutationBatch();
			clm = m.withRow(cf, event.getId().toString());
		}

		@Override
		public Mutation addBet(BetId betId, AccountId accountId) {
			clm.putColumn(new EventComposite(EventComposite.BET_TYPE, betId.toString()), accountId.toString());
			super.addBet(betId, accountId);
			return this;
		}

		@Override
		public Mutation settleBet(BetId betId) {
			clm.putColumn(new EventComposite(EventComposite.SETTLEMENT_TYPE, betId.toString()), EMPTY);
			super.settleBet(betId);
			return this;
		}

		@Override
		public Mutation closeEvent() {
			clm.putColumn(new EventComposite(EventComposite.OTHER, IS_CLOSED), true);
			super.closeEvent();
			return this;
		}

		@Override
		public void apply() throws StoreException {
			try {
				m.execute();
				super.apply();
			} catch (ConnectionException e) {
				throw new StoreException(e);
			}
		}
	}

	private final Keyspace ks;
	private final ColumnFamily<String, EventComposite> cf;

	private final Map<EventId, Event> events = new ConcurrentHashMap<>();
	private final AccountManager accountManager;
	private final CassandraEventArchiver archiver;

	public CassandraEventManager(Keyspace ks, AccountManager accountManager) throws ConnectionException {
		if (ks == null) {
			throw new IllegalArgumentException("ks is required");
		}

		if (accountManager == null) {
			throw new IllegalArgumentException("accountManager is required");
		}

		this.ks = ks;
		this.archiver = new CassandraEventArchiver(ks);

		cf = new ColumnFamily<>(CF, StringSerializer.get(), eventSerializer);
		this.accountManager = accountManager;

		if (ks.describeKeyspace().getColumnFamily(CF) == null) {
			ks.createColumnFamily(cf, ImmutableMap.<String, Object>builder()
					.put("default_validation_class", "UTF8Type")
					.put("key_validation_class", "UTF8Type")
					.put("comparator_type", "CompositeType(LongType, UTF8Type)")
					.build());
		}

		// load all known events
		Rows<String, EventComposite> rows;
		try {
			rows = ks.prepareQuery(cf)
					.getAllRows()
					.setRowLimit(50)
					.setExceptionCallback(new ExceptionCallback() {
						@Override
						public boolean onException(ConnectionException e) {
							logger.error("Unable to connect to cassandra", e);
							return false; // don't bother retrying
						}
					})
					.execute().getResult();
		} catch (ConnectionException e) {
			logger.error("Unable to connect to cassandra", e);
			throw new RuntimeException(e);
		}

		int count = 0;
		for (Row<String, EventComposite> row : rows) {
			Event event = loadFromRow(new EventId(row.getKey()), row.getColumns());
			if (event != null) {
				count++;
				events.put(event.getId(), event);
			}
		}
		logger.info("Loaded {} events from storage", count);

		ScheduledExecutorService eventCleaner = Executors.newSingleThreadScheduledExecutor();
		eventCleaner.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				int count = 0;
				for (Event event : events.values()) {
					try {
						if (event.isClosed() && event.getBets().isEmpty()) {
							archiver.archive(event);
							remove(event.getId());
							count++;
						}
					} catch (Exception e) {
						logger.warn("Unable to archive event {}", event.getId(), e);
					}
				}

				if (count > 0) {
					logger.info("Archived {} events", count);
				}
			}
		}, POLL_TIME, POLL_TIME, POLL_TIME_UNIT);
	}

	private Event loadFromRow(EventId eventId, ColumnList<EventComposite> columns) {
		if (!columns.isEmpty()) {
			boolean isClosed = false;
			String game = null;

			Map<BetId, AccountId> bets = new HashMap<>();
			Set<BetId> settledBets = new HashSet<>();
			for (Column<EventComposite> column : columns) {
				if (column.getName().type == EventComposite.OTHER) {
					if (column.getName().id.equals(IS_CLOSED)) {
						isClosed = column.getBooleanValue();
					} else if (column.getName().id.equals(GAME)) {
						game = column.getStringValue();
					} else {
						logger.warn("Unexpected event column: {}", column.getName().id);
					}
				} else if (column.getName().type == EventComposite.BET_TYPE) {
					bets.put(new BetId(column.getName().id), new AccountId(column.getValue(StringSerializer.get())));
				} else if (column.getName().type == EventComposite.SETTLEMENT_TYPE) {
					settledBets.add(new BetId(column.getName().id));
				} else {
					logger.warn("Unexpected event type: {}", column.getName().type);
				}
			}

			Event event = new Event(accountManager, CassandraEventManager.this, eventId, game);
			event.setBets(bets);
			event.setSettledBets(settledBets);
			event.setIsClosed(isClosed);

			return event;
		}

		return null;
	}

	@Override
	public Event create(String game) throws StoreException {
		try {
			EventId id = EventId.randomEventId();

			MutationBatch m = ks.prepareMutationBatch();
			m.withRow(cf, id.toString())
					.putColumn(new EventComposite(EventComposite.OTHER, GAME), game)
					.putColumn(new EventComposite(EventComposite.OTHER, IS_CLOSED), false);

			m.execute();

			Event event = new Event(accountManager, this, id, game);
			events.put(id, event);

			return event;
		} catch (ConnectionException e) {
			throw new StoreException(e);
		}
	}

	@Override
	public Event get(EventId id) throws LoadException {
		return events.get(id);
	}

	@Override
	public List<Event> get(String game) throws LoadException {
		if (Strings.isNullOrEmpty(game)) {
			throw new IllegalArgumentException("game is required");
		}

		List<Event> gameEvents = new ArrayList<>();
		for (Event e : events.values()) {
			if (e.getGame().equals(game)) {
				gameEvents.add(e);
			}
		}

		return gameEvents;
	}

	private void remove(EventId id) throws StoreException {
		Event event = events.remove(id);

		if (event != null) {
			try {
				MutationBatch m = ks.prepareMutationBatch();
				m.withRow(cf, id.toString()).delete();
				m.execute();
			} catch (ConnectionException e) {
				throw new StoreException(e);
			}
		}
	}

	@Override
	public Mutation mutate(Event event) {
		return new EventMutation(event);
	}
}
