/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.data.events;

import com.google.common.collect.ImmutableMap;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.MutationBatch;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.serializers.StringSerializer;
import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.model.events.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CassandraEventArchiver {
	private static final Logger logger = LoggerFactory.getLogger(CassandraEventArchiver.class);

	private static final String CF = "eventsArchive";

	private static final String GAME = "game";
	private static final String IS_CLOSED = "isClosed";

	private final Keyspace ks;
	private final ColumnFamily<String, String> cf;

	public CassandraEventArchiver(Keyspace ks) throws ConnectionException {
		if (ks == null) {
			throw new IllegalArgumentException("ks is required");
		}

		this.ks = ks;

		cf = new ColumnFamily<>(CF, StringSerializer.get(), StringSerializer.get());

		if (ks.describeKeyspace().getColumnFamily(CF) == null) {
			ks.createColumnFamily(cf, ImmutableMap.<String, Object>builder()
					.put("default_validation_class", "UTF8Type")
					.put("key_validation_class", "UTF8Type")
					.put("comparator_type", "CompositeType(LongType, UTF8Type)")
					.build());
		}
	}

	public void archive(Event event) throws StoreException {
		try {
			MutationBatch m = ks.prepareMutationBatch();
			m.withRow(cf, event.getId().toString())
					.putColumn(GAME, event.getGame())
					.putColumn(IS_CLOSED, event.isClosed());

			m.execute();
		} catch (ConnectionException e) {
			throw new StoreException(e);
		}
	}
}
