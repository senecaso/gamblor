/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.data.accounts;

import com.purplecritter.gamblor.bookie.exceptions.StoreException;
import com.purplecritter.gamblor.bookie.model.accounts.AbstractAccountManager;
import com.purplecritter.gamblor.bookie.model.accounts.AbstractAccountMutation;
import com.purplecritter.gamblor.bookie.model.accounts.Account;
import com.purplecritter.gamblor.bookie.model.accounts.AccountId;
import com.purplecritter.gamblor.bookie.processor.PaymentProcessor;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MockAccountManager extends AbstractAccountManager {

	private class MockMutation extends AbstractAccountMutation {
		public MockMutation(Account account) {
			super(account);
		}
	}

	private final PaymentProcessor processor;

	private final Map<AccountId, Account> accountCache = new HashMap<>();

	public MockAccountManager(PaymentProcessor processor) {
		super(processor);

		this.processor = processor;
	}

	@Override
	protected Collection<Account> getAccounts() {
		return Collections.unmodifiableCollection(accountCache.values());
	}

	@Override
	public Account create(String returnAddress) {
		if (returnAddress == null || returnAddress.trim().isEmpty()) {
			throw new IllegalArgumentException("invalid returnAddress");
		}

		AccountId id = AccountId.randomAccountId();
		String depositAddress = processor.createAddress();

		Account account = new Account(processor, this, id, depositAddress, returnAddress);
		accountCache.put(id, account);

		return account;
	}

	@Override
	public Account get(AccountId id) {
		return accountCache.get(id);
	}

	@Override
	public void remove(AccountId id) throws StoreException {
		accountCache.remove(id);
	}

	@Override
	public Mutation mutate(Account account) {
		return new MockMutation(account);
	}
}
