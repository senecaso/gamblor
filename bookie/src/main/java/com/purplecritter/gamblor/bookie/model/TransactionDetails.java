/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.bookie.model;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;

public class TransactionDetails {
	private final String id;
	private final Map<String, BigDecimal> outputAmounts;
	private final int confirmations;
	private final int height;

	public TransactionDetails(String id, Map<String, BigDecimal> outputAmounts, int confirmations, int height) {
		if (id == null || id.trim().isEmpty()) {
			throw new IllegalArgumentException("id is required");
		}

		if (outputAmounts == null || outputAmounts.isEmpty()) {
			throw new IllegalArgumentException("invalid outputAmounts");
		}

		this.id = id;
		this.outputAmounts = outputAmounts;
		this.confirmations = confirmations;
		this.height = height;
	}

	public String getId() {
		return id;
	}

	public BigDecimal getAmount(String address) {
		return outputAmounts.get(address);
	}

	public Map<String, BigDecimal> getAmounts() {
		return Collections.unmodifiableMap(outputAmounts);
	}

	public int getConfirmations() {
		return confirmations;
	}

	public int getHeight() {
		return height;
	}
}
