# WARNING

This is a partial implementation of a bitcoin bet processing engine, with a couple of games built on top of it.  The code as it is is not functional.  If you decide to use it, you will need to fix the remaining issues, add any missing features, and figure out how to legally deploy it in your jurisdiction.  I am not a lawyer, so I cannot help you with this; please don't contact me for support.  You're on your own.

## Architecture

The system was built using the following technologies:

- [BitsOfProof](https://github.com/bitsofproof/supernode)
- [Apache Apollo](http://activemq.apache.org/apollo/)
- [Cassandra](http://cassandra.apache.org/)

BitsOfProof (BOP) is used for transaction processing and wallet support.  Apollo is the message broker used to communicate between BOP and bookie.  Cassandra is used to store all bet and account data.

The basic idea was to build a session-based bet processing engine that didn't require users to login, but would allow them to place deposits which could be used within the "casino" on each of the various games to place their bets.  The implementation needed to be fault tolerant so that if the system went down, no bets would be lost and the system could recover simply by coming back online.  This is achieved using Cassandra to persist all data.

A typical deployment would involve at least 3 Cassandra instances so that a replication factor of 3 could be used to provide fault tolerance.  Ideally, more than 3 instances would be used so that if a single node went down it wouldnt stop game-play.  It would also include a single Apollo instance used to communicate with BOP, and a single instance of the BOP supernode.  I haven't looked into how BOP and Apollo are made HA, so you will have to figure that out on your own.

Included in the utils project is a small python script (diceTest.py) which demonstrates how a UI would interact with the dice game.  By spinning up a large number of these, you can perform simple load testing on the system.  Obviously something more automated will be better, but this is a starting point.

No security has been built into the system at all.  Currently, all deposits are held in the hot wallet, which means this will be a target for attack.  bessy, apollo, the BOP supernode, and Cassandra need to be protected, so they need to be considered to be operating on a trusted network.

In preliminary testing, with everything running on the same quad-core machine, I was able to achieve up to 300 bets/s, but the maintainable load was closer to 100 bets/s.  Of course, YMMV, especially when you consider the amount of features which are missing.

## Deployment

### Apollo & BOP
Follow the documentation provided by BOP.  These 2 need to be running before bessy can be started.

### Cassandra
Follow the documentation provided by Cassandra.  If you want to run 3 instances on the same machine, you will need to create a virtual interface for each instance.  This is because Cassandra nodes all bind to the same port.  Ideally you would deploy 1 instance per machine though, otherwise you aren't really protected from failure.  I have been using the default configuration so far, but you can probably achieve better performance if you tweak the settings.  Cassandra should be running before you start bessy

### bessy
bessy is a simple REST server, so you just need to run BessyServer and it will connect to Apollo and Cassandra.


## What's missing

- user bet history
- game bet history
- testing; very little has been done
- auditing
- monthly reporting
- hot/cold wallet
- sharding/scalability
- performance tuning
- the main method has everything hardcoded and is pointing to testnet
- no support for progressives, only an initial attempt was made
- security features like locking down bessy to only accept trusted requests