/**
  * Copyright (c) 2013 Shaun Senecal
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */

package com.purplecritter.gamblor.test;

import com.google.common.collect.ImmutableMap;
import com.netflix.astyanax.AstyanaxContext;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.connectionpool.NodeDiscoveryType;
import com.netflix.astyanax.connectionpool.impl.ConnectionPoolConfigurationImpl;
import com.netflix.astyanax.connectionpool.impl.ConnectionPoolType;
import com.netflix.astyanax.connectionpool.impl.CountingConnectionPoolMonitor;
import com.netflix.astyanax.ddl.ColumnFamilyDefinition;
import com.netflix.astyanax.ddl.KeyspaceDefinition;
import com.netflix.astyanax.impl.AstyanaxConfigurationImpl;
import com.netflix.astyanax.thrift.ThriftFamilyFactory;
import com.netflix.astyanax.util.SingletonEmbeddedCassandra;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public abstract class BaseCassandraTest {
	private static final long CASSANDRA_WAIT_TIME = 3000;
	private static final String SEEDS = "localhost:9160";
	private static String TEST_CLUSTER_NAME  = "cass_sandbox";
	private static String TEST_KEYSPACE_NAME = "TestCassandraAccount";

	protected static Keyspace keyspace;
	private static AstyanaxContext<Keyspace> keyspaceContext;

	@BeforeClass
	public static void setup() throws Exception {
		System.out.println("TESTING THRIFT KEYSPACE");

		SingletonEmbeddedCassandra.getInstance();

		Thread.sleep(CASSANDRA_WAIT_TIME);

		createKeyspace();
	}

	@AfterClass
	public static void teardown() throws Exception {
		if (keyspaceContext != null)
			keyspaceContext.shutdown();

		Thread.sleep(CASSANDRA_WAIT_TIME);
	}

	public static void createKeyspace() throws Exception {
		keyspaceContext = new AstyanaxContext.Builder()
				.forCluster(TEST_CLUSTER_NAME)
				.forKeyspace(TEST_KEYSPACE_NAME)
				.withAstyanaxConfiguration(
						new AstyanaxConfigurationImpl()
								.setDiscoveryType(NodeDiscoveryType.RING_DESCRIBE)
								.setConnectionPoolType(ConnectionPoolType.TOKEN_AWARE))
				.withConnectionPoolConfiguration(
						new ConnectionPoolConfigurationImpl(TEST_CLUSTER_NAME
								+ "_" + TEST_KEYSPACE_NAME)
								.setSocketTimeout(30000)
								.setMaxTimeoutWhenExhausted(2000)
								.setMaxConnsPerHost(10)
								.setInitConnsPerHost(10)
								.setSeeds(SEEDS))
				.withConnectionPoolMonitor(new CountingConnectionPoolMonitor())
				.buildKeyspace(ThriftFamilyFactory.getInstance());

		keyspaceContext.start();

		keyspace = keyspaceContext.getEntity();

		try {
			keyspace.dropKeyspace();
		} catch (Exception e) {

		}

		keyspace.createKeyspace(ImmutableMap.<String, Object>builder()
				.put("strategy_options", ImmutableMap.<String, Object>builder()
						.put("replication_factor", "1")
						.build())
				.put("strategy_class", "SimpleStrategy")
				.build()
		);
	}

	@After
	public void after() throws Exception {
		KeyspaceDefinition ksDefn = keyspace.describeKeyspace();
		for (ColumnFamilyDefinition cfDef : ksDefn.getColumnFamilyList()) {
			keyspace.truncateColumnFamily(cfDef.getName());
		}
	}
}
